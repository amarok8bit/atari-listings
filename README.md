# Atari Listings

Atari 8-bit listings typed from various sources.

## Table of contents

| Title | Year | Issues | 
| ------ | ------ | ------ |
| Bajtek | 1986 | [5-6](#bajtek-1986-5-6), [10](#bajtek-1986-10), [11](#bajtek-1986-11), [12](#bajtek-1986-12) |
| Bajtek | 1987 | [1](#bajtek-1987-1), [2](#bajtek-1987-2), [3](#bajtek-1987-3), [4](#bajtek-1987-4), [5](#bajtek-1987-5), [6](#bajtek-1987-6), [7](#bajtek-1987-7), [8](#bajtek-1987-8), [9](#bajtek-1987-9), [10](#bajtek-1987-10), [11](#bajtek-1987-11), [12](#bajtek-1987-12), [Tylko o Atari](#bajtek-1987-tylko-o-atari) |
| Bajtek | 1988 | [1](#bajtek-1988-1), [2](#bajtek-1988-2), [3](#bajtek-1988-3), [4](#bajtek-1988-4), [5](#bajtek-1988-5), [6](#bajtek-1988-6), [7](#bajtek-1988-7), [8](#bajtek-1988-8), [9](#bajtek-1988-9), [10](#bajtek-1988-10), [11](#bajtek-1988-11), [12](#bajtek-1988-12), [Tylko o Atari](#bajtek-1988-tylko-o-atari) |
| Bajtek | 1989 | [1](#bajtek-1989-1), [2](#bajtek-1989-2), [4](#bajtek-1989-4), [5](#bajtek-1989-5), [6](#bajtek-1989-6), [7](#bajtek-1989-7), [8](#bajtek-1989-8), [9](#bajtek-1989-9), [10](#bajtek-1989-10), [11](#bajtek-1989-11), [12](#bajtek-1989-12), [Tylko dla początkujących](#bajtek-1989-tylko-dla-początkujących) |
| Bajtek | 1990 | [1-2](#bajtek-1990-1-2), [3-4](#bajtek-1990-3-4), [5-6](#bajtek-1990-5-6), [7-8](#bajtek-1990-7-8), [9-10](#bajtek-1990-9-10), [11-12](#bajtek-1990-11-12) |
| Bajtek | 1991 | [1](#bajtek-1991-1), [2](#bajtek-1991-2), [3](#bajtek-1991-3), [4](#bajtek-1991-4), [5](#bajtek-1991-5), [6](#bajtek-1991-6), [7](#bajtek-1991-7), [8](#bajtek-1991-8), [9](#bajtek-1991-9), [10](#bajtek-1991-10), [12](#bajtek-1991-12) |
| Bajtek | 1992 | [1](#bajtek-1992-1), [5](#bajtek-1992-5), [6](#bajtek-1992-6), [8](#bajtek-1992-8), [11](#bajtek-1992-11) |
| Bajtek | 1993 | [3](#bajtek-1993-3), [4](#bajtek-1993-4), [5](#bajtek-1993-5), [6](#bajtek-1993-6), [7](#bajtek-1993-7), [8-9](#bajtek-1993-8-9), [10](#bajtek-1993-10), [12](#bajtek-1993-12) |
| Bajtek | 1994 | [2](#bajtek-1994-2) |
| Bajtek | 1995 | [3](#bajtek-1995-3) |
| Komputer | 1986 | [8](#komputer-1986-8), [9](#komputer-1986-9) |
| Komputer | 1987 | [2](#komputer-1987-2), [11](#komputer-1987-11), [12](#komputer-1987-12) |
| Komputer | 1988 | [1](#komputer-1988-1), [2](#komputer-1988-2), [3](#komputer-1988-3), [4](#komputer-1988-4), [5](#komputer-1988-5), [6](#komputer-1988-6), [7](#komputer-1988-7), [9](#komputer-1988-9), [10](#komputer-1988-10), [11](#komputer-1988-11), [12](#komputer-1988-12)  |
| Komputer | 1989 | [1](#komputer-1989-1), [2](#komputer-1989-2), [3](#komputer-1989-3), [4](#komputer-1989-4) |
| Moje Atari | 1990 | [1](#moje-atari-1990-1), [2](#moje-atari-1990-2) |
| Moje Atari | 1991 | [3](#moje-atari-1991-3), [4](#moje-atari-1991-4), [5](#moje-atari-1991-5), [6](#moje-atari-1991-6), [7](#moje-atari-1991-7) |

## Bajtek

All scanned issues of **Bajtek** magazine are available on [atarionline.pl](http://atarionline.pl/v01/index.php?subaction=showfull&id=1234027498&archive=&start_from=0&ucat=8&ct=biblioteka#zin=Bajtek__rok=Wszystkie)\
All listings stored on Atari disk images are available here: [bajtek.zip](Bajtek/bajtek.zip)

### Bajtek 1986 / 5-6

![Bajtek 1986 / 5-6](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1986_05_06_male.jpg)

All listings on Atari disk image: [bajtek_1986_5_6.atr](Bajtek/1986/5-6/bajtek_1986_5_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 19 | Animacja | Marek Kuliński | [ANIMACJA.LST](Bajtek/1986/5-6/ANIMACJA.LST), [ANIMACJA.BAS](Bajtek/1986/5-6/ANIMACJA.BAS) |
| 20 | Renumeracja programów w języku BASIC | Mariusz J. Giergiel | [RENUM.LST](Bajtek/1986/5-6/RENUM.LST), [RENUM.BAS](Bajtek/1986/5-6/RENUM.BAS) |
| 20 | Weryfikacja programów na kasecie | Mariusz J. Giergiel | [WERYFIK.LST](Bajtek/1986/5-6/WERYFIK.LST), [WERYFIK.BAS](Bajtek/1986/5-6/WERYFIK.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1986 / 10

![Bajtek 1986 / 10](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1986_10_male.jpg)

All listings on Atari disk image: [bajtek_1986_10.atr](Bajtek/1986/10/bajtek_1986_10.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 12 | Zegar | Marek Markowski | [ZEGAR.LST](Bajtek/1986/10/ZEGAR.LST), [ZEGAR.BAS](Bajtek/1986/10/ZEGAR.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1986 / 11

![Bajtek 1986 / 11](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1986_11_male.jpg)

All listings on Atari disk image: [bajtek_1986_11.atr](Bajtek/1986/11/bajtek_1986_11.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 14 | Tape Copier | Dariusz Adamowski | [TAPECOPY.LST](Bajtek/1986/11/TAPECOPY.LST), [TAPECOPY.BAS](Bajtek/1986/11/TAPECOPY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1986 / 12

![Bajtek 1986 / 12](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1986_12_male.jpg)

All listings on Atari disk image: [bajtek_1986_12.atr](Bajtek/1986/12/bajtek_1986_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 13 | Świat dźwięków Atari | Mariusz J. Giergiel | [EKSPLOZJ.LST](Bajtek/1986/12/EKSPLOZJ.LST), [EKSPLOZJ.BAS](Bajtek/1986/12/EKSPLOZJ.BAS), [BRONMASZ.LST](Bajtek/1986/12/BRONMASZ.LST), [BRONMASZ.BAS](Bajtek/1986/12/BRONMASZ.BAS), [STRZALY.LST](Bajtek/1986/12/STRZALY.LST), [STRZALY.BAS](Bajtek/1986/12/STRZALY.BAS), [BOMBY.LST](Bajtek/1986/12/BOMBY.LST), [BOMBY.BAS](Bajtek/1986/12/BOMBY.BAS), [FAJERWER.LST](Bajtek/1986/12/FAJERWER.LST), [FAJERWER.BAS](Bajtek/1986/12/FAJERWER.BAS), [KARETKA.LST](Bajtek/1986/12/KARETKA.LST), [KARETKA.BAS](Bajtek/1986/12/KARETKA.BAS), [RADIOWOZ.LST](Bajtek/1986/12/RADIOWOZ.LST), [RADIOWOZ.BAS](Bajtek/1986/12/RADIOWOZ.BAS), [STRAZPOZ.LST](Bajtek/1986/12/STRAZPOZ.LST), [STRAZPOZ.BAS](Bajtek/1986/12/STRAZPOZ.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 1

![Bajtek 1987 / 1](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_01_male.jpg)

All listings on Atari disk image: [bajtek_1987_1.atr](Bajtek/1987/1/bajtek_1987_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 10 | Nessie | Janusz Wiśniewski | [NESSIE.LST](Bajtek/1987/1/NESSIE.LST), [NESSIE.BAS](Bajtek/1987/1/NESSIE.BAS) |
| 11 | Nie bój się przerwań | Wojciech Zientara | [PRZERWAN.LST](Bajtek/1987/1/PRZERWAN.LST), [PRZERWAN.BAS](Bajtek/1987/1/PRZERWAN.BAS) |
| 12 | Teksty w trybie graficznym 8 | Wojciech Zientara | [TEKSTY.LST](Bajtek/1987/1/TEKSTY.LST), [TEKSTY.BAS](Bajtek/1987/1/TEKSTY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 2

![Bajtek 1987 / 2](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_02_male.jpg)

All listings on Atari disk image: [bajtek_1987_2.atr](Bajtek/1987/2/bajtek_1987_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 7 | Symulator 6502 | Wojciech Zientara | [SYM6502.LST](Bajtek/1987/2/SYM6502.LST), [SYM6502.BAS](Bajtek/1987/2/SYM6502.BAS) |
| 9 | Spis zawartości dyskietki i usuwanie plików bez DOS-u | (ziew) | [DIR.LST](Bajtek/1987/2/DIR.LST), [DIR.BAS](Bajtek/1987/2/DIR.BAS), [USUW.LST](Bajtek/1987/2/USUW.LST), [USUW.BAS](Bajtek/1987/2/USUW.BAS) |
| 9 | Nie bój się przerwań | Wojciech Zientara | [BREAK.LST](Bajtek/1987/2/BREAK.LST), [BREAK.BAS](Bajtek/1987/2/BREAK.BAS) |
| 10 | Polskie znaki | Krzysztof Leski | [POLSKIE.LST](Bajtek/1987/2/POLSKIE.LST), [POLSKIE.BAS](Bajtek/1987/2/POLSKIE.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 3

![Bajtek 1987 / 3](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_03_male.jpg)

All listings on Atari disk image: [bajtek_1987_3.atr](Bajtek/1987/3/bajtek_1987_3.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 8 | Duszki | Wojciech Zientara | [DUSZKI.LST](Bajtek/1987/3/DUSZKI.LST), [DUSZKI.BAS](Bajtek/1987/3/DUSZKI.BAS) |
| 9 | Nie bój się przerwań | (ziew) | [PRZERWAN.LST](Bajtek/1987/3/PRZERWAN.LST), [PRZERWAN.BAS](Bajtek/1987/3/PRZERWAN.BAS) |
| 10 | Polskie znaki | Krzysztof Leski | [POLSKIE1.LST](Bajtek/1987/3/POLSKIE1.LST), [POLSKIE1.BAS](Bajtek/1987/3/POLSKIE1.BAS), [POLSK2I3.LST](Bajtek/1987/3/POLSK2I3.LST), [POLSK2I3.BAS](Bajtek/1987/3/POLSK2I3.BAS), [POLSK3I4.LST](Bajtek/1987/3/POLSK3I4.LST), [POLSK3I4.BAS](Bajtek/1987/3/POLSK3I4.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 4

![Bajtek 1987 / 4](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_04_male.jpg)

All listings on Atari disk image: [bajtek_1987_4.atr](Bajtek/1987/4/bajtek_1987_4.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Duszki | Wojciech Zientara | [DUSZKI1.LST](Bajtek/1987/4/DUSZKI1.LST), [DUSZKI1.BAS](Bajtek/1987/4/DUSZKI1.BAS), [DUSZKI2.LST](Bajtek/1987/4/DUSZKI2.LST), [DUSZKI2.BAS](Bajtek/1987/4/DUSZKI2.BAS), [DUSZKI3.LST](Bajtek/1987/4/DUSZKI3.LST), [DUSZKI3.BAS](Bajtek/1987/4/DUSZKI3.BAS) |
| 5 | Nie bój się przerwań | (ziew) | [PRZERWAN.LST](Bajtek/1987/4/PRZERWAN.LST), [PRZERWAN.BAS](Bajtek/1987/4/PRZERWAN.BAS) |
| 6 | Start dom | Wojciech Zientara | [STARYDOM.LST](Bajtek/1987/4/STARYDOM.LST), [STARYDOM.BAS](Bajtek/1987/4/STARYDOM.BAS) |
| 7 | Całkowanie numeryczne | (ziew) | [CALKA.LST](Bajtek/1987/4/CALKA.LST), [CALKA.BAS](Bajtek/1987/4/CALKA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 5

![Bajtek 1987 / 5](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_05_male.jpg)

All listings on Atari disk image: [bajtek_1987_5.atr](Bajtek/1987/5/bajtek_1987_5.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Ciągi tekstowe | Wojciech Zientara | [CIAGI1.LST](Bajtek/1987/5/CIAGI1.LST), [CIAGI1.BAS](Bajtek/1987/5/CIAGI1.BAS), [CIAGI2.LST](Bajtek/1987/5/CIAGI2.LST), [CIAGI2.BAS](Bajtek/1987/5/CIAGI2.BAS), [CIAGI3.LST](Bajtek/1987/5/CIAGI3.LST), [CIAGI3.BAS](Bajtek/1987/5/CIAGI3.BAS), [CIAGI4.LST](Bajtek/1987/5/CIAGI4.LST), [CIAGI4.BAS](Bajtek/1987/5/CIAGI4.BAS) |
| 8 | Kasetowy System Operacyjny | Wojciech Zabołotny | [KSO.LST](Bajtek/1987/5/KSO.LST), [KSO.BAS](Bajtek/1987/5/KSO.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 6

![Bajtek 1987 / 6](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_06_male.jpg)

All listings on Atari disk image: [bajtek_1987_6.atr](Bajtek/1987/6/bajtek_1987_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 10 | Nie bój się przewań... i duszków | (ziew) | [DUSZKI.LST](Bajtek/1987/6/DUSZKI.LST), [DUSZKI.BAS](Bajtek/1987/6/DUSZKI.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 7

![Bajtek 1987 / 7](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_07_male.jpg)

All listings on Atari disk image: [bajtek_1987_7.atr](Bajtek/1987/7/bajtek_1987_7.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 8 | Płynący napis | Andrzej Biazik | [NAPIS.LST](Bajtek/1987/7/NAPIS.LST), [NAPIS.BAS](Bajtek/1987/7/NAPIS.BAS) |
| 9 | Zamiana napisów w programach | Wojciech Zientara | [ZAMIANA.LST](Bajtek/1987/7/ZAMIANA.LST), [ZAMIANA.BAS](Bajtek/1987/7/ZAMIANA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 8

![Bajtek 1987 / 8](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_08_male.jpg)

All listings on Atari disk image: [bajtek_1987_8.atr](Bajtek/1987/8/bajtek_1987_8.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 8 | Speed trans | Marek Drążkiewicz | [SPEEDTRA.LST](Bajtek/1987/8/SPEEDTRA.LST), [SPEEDTRA.BAS](Bajtek/1987/8/SPEEDTRA.BAS) |
| 9 | Wglądownica | Janusz B. Wiśniewski | [WGLAD.LST](Bajtek/1987/8/WGLAD.LST), [WGLAD.BAS](Bajtek/1987/8/WGLAD.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 9

![Bajtek 1987 / 9](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_09_male.jpg)

All listings on Atari disk image: [bajtek_1987_9.atr](Bajtek/1987/9/bajtek_1987_9.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 10 | Klawisze funkcyjne | Tomasz Bigaj | [KLAWISZE.LST](Bajtek/1987/9/KLAWISZE.LST), [KLAWISZE.BAS](Bajtek/1987/9/KLAWISZE.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 10

![Bajtek 1987 / 10](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_10_male.jpg)

All listings on Atari disk image: [bajtek_1987_10.atr](Bajtek/1987/10/bajtek_1987_10.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 7 | Generator rytmów | Janusz B. Wiśniewski | [RYTMY.LST](Bajtek/1987/10/RYTMY.LST), [RYTMY.BAS](Bajtek/1987/10/RYTMY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 11

![Bajtek 1987 / 11](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_11_male.jpg)

All listings on Atari disk image: [bajtek_1987_11.atr](Bajtek/1987/11/bajtek_1987_11.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | BASIC XE | Mariusz J. Giergiel | [LOCAL.LST](Bajtek/1987/11/LOCAL.LST), [LOCAL.BAS](Bajtek/1987/11/LOCAL.BAS), [WHILE.LST](Bajtek/1987/11/WHILE.LST), [WHILE.BAS](Bajtek/1987/11/WHILE.BAS), [IF.LST](Bajtek/1987/11/IF.LST), [IF.BAS](Bajtek/1987/11/IF.BAS), [GOSUB.LST](Bajtek/1987/11/GOSUB.LST), [GOSUB.BAS](Bajtek/1987/11/GOSUB.BAS), [PROCEDUR.LST](Bajtek/1987/11/PROCEDUR.LST), [PROCEDUR.BAS](Bajtek/1987/11/PROCEDUR.BAS), [PMG1.LST](Bajtek/1987/11/PMG1.LST), [PMG1.BAS](Bajtek/1987/11/PMG1.BAS), [PMG2.LST](Bajtek/1987/11/PMG2.LST), [PMG2.BAS](Bajtek/1987/11/PMG2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / 12

![Bajtek 1987 / 12](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1987_12_male.jpg)

All listings on Atari disk image: [bajtek_1987_12.atr](Bajtek/1987/12/bajtek_1987_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 11 | Duszek raz jeszcze | Leszek Paprocki | [DUSZEK.LST](Bajtek/1987/12/DUSZEK.LST), [DUSZEK.BAS](Bajtek/1987/12/DUSZEK.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1987 / Tylko o Atari

![Bajtek 1987 / Tylko o Atari](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_Tylko_o_Atari_01_male.jpg)

All listings on Atari disk image: [bajtek_1987_tylko_o_atari.atr](Bajtek/1987/Tylko_o_Atari/bajtek_1987_tylko_o_atari.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 3 | Edytor BASICA-a | | [EDYTBAS.LST](Bajtek/1987/Tylko_o_Atari/EDYTBAS.LST), [EDYTBAS.BAS](Bajtek/1987/Tylko_o_Atari/EDYTBAS.BAS) |
| 9 | Print Shop Converter | Wojciech Zientara | [PSCONV.LST](Bajtek/1987/Tylko_o_Atari/PSCONV.LST), [PSCONV.BAS](Bajtek/1987/Tylko_o_Atari/PSCONV.BAS) |
| 13 | Ocena - Zegar | Wojciech Zachariasz, Wojciech Zientara | [ZEGAR1.LST](Bajtek/1987/Tylko_o_Atari/ZEGAR1.LST), [ZEGAR1.BAS](Bajtek/1987/Tylko_o_Atari/ZEGAR1.BAS), [ZEGAR2.LST](Bajtek/1987/Tylko_o_Atari/ZEGAR2.LST), [ZEGAR2.BAS](Bajtek/1987/Tylko_o_Atari/ZEGAR2.BAS) |
| 14 | Turbo Copy | Dariusz Modrinić | [TURBOCPY.LST](Bajtek/1987/Tylko_o_Atari/TURBOCPY.LST), [TURBOCPY.BAS](Bajtek/1987/Tylko_o_Atari/TURBOCPY.BAS) |
| 28 | Biorytmy | Stanisław Jawor | [BIORYTMY.LST](Bajtek/1987/Tylko_o_Atari/BIORYTMY.LST), [BIORYTMY.BAS](Bajtek/1987/Tylko_o_Atari/BIORYTMY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 1

![Bajtek 1988 / 1](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_01_male.jpg)

All listings on Atari disk image: [bajtek_1988_1.atr](Bajtek/1988/1/bajtek_1988_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 12 | Edytor BASIC-a | | [EDYTBAS.LST](Bajtek/1988/1/EDYTBAS.LST), [EDYTBAS.BAS](Bajtek/1988/1/EDYTBAS.BAS) |
| 13 | Który lepszy | Wojciech Zientara | [TEST1.LST](Bajtek/1988/1/TEST1.LST), [TEST1.BAS](Bajtek/1988/1/TEST1.BAS), [TEST2.LST](Bajtek/1988/1/TEST2.LST), [TEST2.BAS](Bajtek/1988/1/TEST2.BAS), [TEST3.LST](Bajtek/1988/1/TEST3.LST), [TEST3.BAS](Bajtek/1988/1/TEST3.BAS), [TEST4.LST](Bajtek/1988/1/TEST4.LST), [TEST4.BAS](Bajtek/1988/1/TEST4.BAS), [TEST5.LST](Bajtek/1988/1/TEST5.LST), [TEST5.BAS](Bajtek/1988/1/TEST5.BAS), [TEST6.LST](Bajtek/1988/1/TEST6.LST), [TEST6.BAS](Bajtek/1988/1/TEST6.BAS), [TEST7.LST](Bajtek/1988/1/TEST7.LST), [TEST7.BAS](Bajtek/1988/1/TEST7.BAS), [TEST8.LST](Bajtek/1988/1/TEST8.LST), [TEST8.BAS](Bajtek/1988/1/TEST8.BAS), [TEST9.LST](Bajtek/1988/1/TEST9.LST), [TEST9.BAS](Bajtek/1988/1/TEST9.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 2

![Bajtek 1988 / 2](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_02_male.jpg)

All listings on Atari disk image: [bajtek_1988_2.atr](Bajtek/1988/2/bajtek_1988_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 7 | Porty joysticków | Przemysław Strzelecki, Wojciech Zientara | [PORTY.LST](Bajtek/1988/2/PORTY.LST), [PORTY.BAS](Bajtek/1988/2/PORTY.BAS) |
| 8 | Migający kursor | Bartosz Polednia, Adam Hartwig, Marek Krupa | [KURSOR1.LST](Bajtek/1988/2/KURSOR1.LST), [KURSOR1.BAS](Bajtek/1988/2/KURSOR1.BAS), [KURSOR2.LST](Bajtek/1988/2/KURSOR2.LST), [KURSOR2.BAS](Bajtek/1988/2/KURSOR2.BAS), [KURSOR3.LST](Bajtek/1988/2/KURSOR3.LST), [KURSOR3.BAS](Bajtek/1988/2/KURSOR3.BAS) |
| 9 | Atak! | Wojciech Zientara | [ATAK.LST](Bajtek/1988/2/ATAK.LST), [ATAK.BAS](Bajtek/1988/2/ATAK.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 3

![Bajtek 1988 / 3](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_03_male.jpg)

All listings on Atari disk image: [bajtek_1988_3.atr](Bajtek/1988/3/bajtek_1988_3.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | XIO | Wojciech Zientara | [XIO.LST](Bajtek/1988/3/XIO.LST), [XIO.BAS](Bajtek/1988/3/XIO.BAS) |
| 6 | Przesuwanie kursora | Andrzej Holanowski | [KURSOR.LST](Bajtek/1988/3/KURSOR.LST), [KURSOR.BAS](Bajtek/1988/3/KURSOR.BAS) |
| 7 | Grafika w DLI | Wojciech Wylon | [DLI1.LST](Bajtek/1988/3/DLI1.LST), [DLI1.BAS](Bajtek/1988/3/DLI1.BAS), [DLI2.LST](Bajtek/1988/3/DLI2.LST), [DLI2.BAS](Bajtek/1988/3/DLI2.BAS), [DLI2I3.LST](Bajtek/1988/3/DLI2I3.LST), [DLI2I3.BAS](Bajtek/1988/3/DLI2I3.BAS), [DLI4.LST](Bajtek/1988/3/DLI4.LST), [DLI4.BAS](Bajtek/1988/3/DLI4.BAS) |
| 7 | Obsluga klawisza RESET | Jakub Cebula | [RESET.LST](Bajtek/1988/3/RESET.LST), [RESET.BAS](Bajtek/1988/3/RESET.BAS) |
| 8 | Nie tylko dla graczy | Marek Renner | [NIETYLKO.LST](Bajtek/1988/3/NIETYLKO.LST), [NIETYLKO.BAS](Bajtek/1988/3/NIETYLKO.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 4

![Bajtek 1988 / 4](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_04_male.jpg)

All listings on Atari disk image: [bajtek_1988_4.atr](Bajtek/1988/4/bajtek_1988_4.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 11 | Korekta Kyan Pascala | Wojciech Zabołotny | [PRZYKL1.PAS](Bajtek/1988/4/PRZYKL1.PAS), [PRZYKL2.PAS](Bajtek/1988/4/PRZYKL2.PAS), [PROGRAM3.LST](Bajtek/1988/4/PROGRAM3.LST), [PROGRAM3.BAS](Bajtek/1988/4/PROGRAM3.BAS) |
| 11 | Zamiana liczb | Dorota Ciesielska | [ZAMIANA.LST](Bajtek/1988/4/ZAMIANA.LST), [ZAMIANA.BAS](Bajtek/1988/4/ZAMIANA.BAS) |
| 13 | Autostart programów | Tomasz Waligóra | [LOADER.LST](Bajtek/1988/4/LOADER.LST), [LOADER.BAS](Bajtek/1988/4/LOADER.BAS) |
| 14 | Action! znaczy szybkość | Wojciech Zientara | [ACTION.ACT](Bajtek/1988/4/ACTION.ACT), [C.C](Bajtek/1988/4/C.C), [BASIC.LST](Bajtek/1988/4/BASIC.LST), [BASIC.BAS](Bajtek/1988/4/BASIC.BAS), [TBASICXL.LST](Bajtek/1988/4/TBASICXL.LST), [TBASICXL.BAS](Bajtek/1988/4/TBASICXL.BAS), [BASICXE.LST](Bajtek/1988/4/BASICXE.LST), [BASICXE.BAS](Bajtek/1988/4/BASICXE.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 5

![Bajtek 1988 / 5](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_05_male.jpg)

All listings on Atari disk image: [bajtek_1988_5.atr](Bajtek/1988/5/bajtek_1988_5.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 7 | Animacja | Tomasz Banachowicz | [ANIMACJA.LST](Bajtek/1988/5/ANIMACJA.LST), [ANIMACJA.BAS](Bajtek/1988/5/ANIMACJA.BAS) |
| 7 | Kasowanie linii | Marcin Bochenek | [KASOWANI.LST](Bajtek/1988/5/KASOWANI.LST), [KASOWANI.BAS](Bajtek/1988/5/KASOWANI.BAS) |
| 8 | Sortowanie tablicy liczbowej | Andrzej Biazik | [SORT.LST](Bajtek/1988/5/SORT.LST), [SORT.BAS](Bajtek/1988/5/SORT.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 6

![Bajtek 1988 / 6](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_06_male.jpg)

All listings on Atari disk image: [bajtek_1988_6.atr](Bajtek/1988/6/bajtek_1988_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Mini Pacman | Wojciech Zientara | [PACMAN.ACT](Bajtek/1988/6/PACMAN.ACT) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 7

![Bajtek 1988 / 7](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_07_male.jpg)

All listings on Atari disk image: [bajtek_1988_7.atr](Bajtek/1988/7/bajtek_1988_7.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Animacja na 130XE | Wojciech Przybył | [ANIMACJA.LST](Bajtek/1988/7/ANIMACJA.LST), [ANIMACJA.BAS](Bajtek/1988/7/ANIMACJA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 8

![Bajtek 1988 / 8](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_08_male.jpg)

All listings on Atari disk image: [bajtek_1988_8.atr](Bajtek/1988/8/bajtek_1988_8.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 4 | Pianino | Andrzej Biazik | [PIANINO.LST](Bajtek/1988/8/PIANINO.LST), [PIANINO.BAS](Bajtek/1988/8/PIANINO.BAS) |
| 6 | Kropka nad i | Andrzej-Piotr Szyndrowski | [BRKRES.LST](Bajtek/1988/8/BRKRES.LST), [BRKRES.BAS](Bajtek/1988/8/BRKRES.BAS) |
| 6 | Deszyfrator do Sound Machine | Jacek Górski | [DESZYFR.LST](Bajtek/1988/8/DESZYFR.LST), [DESZYFR.BAS](Bajtek/1988/8/DESZYFR.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 9

![Bajtek 1988 / 9](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_09_male.jpg)

All listings on Atari disk image: [bajtek_1988_9.atr](Bajtek/1988/9/bajtek_1988_9.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Mini turbo | Krzysztof Klimczak | [TURBO.LST](Bajtek/1988/9/TURBO.LST), [TURBO.BAS](Bajtek/1988/9/TURBO.BAS) |
| 7 | Kolizje | Maciej Górski | [KOLIZJE.LST](Bajtek/1988/9/KOLIZJE.LST), [KOLIZJE.BAS](Bajtek/1988/9/KOLIZJE.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 10

![Bajtek 1988 / 10](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_10_male.jpg)

All listings on Atari disk image: [bajtek_1988_10.atr](Bajtek/1988/10/bajtek_1988_10.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | 80 znaków w wierszu | Michał Skowroński | [80ZNAKOW.LST](Bajtek/1988/10/80ZNAKOW.LST), [80ZNAKOW.BAS](Bajtek/1988/10/80ZNAKOW.BAS) |
| 7 | Demonstracja grafiki | Arnold Adamczyk | [WYKRES1.LST](Bajtek/1988/10/WYKRES1.LST), [WYKRES1.BAS](Bajtek/1988/10/WYKRES1.BAS), [WYKRES2.LST](Bajtek/1988/10/WYKRES2.LST), [WYKRES2.BAS](Bajtek/1988/10/WYKRES2.BAS), [WYKRES3.LST](Bajtek/1988/10/WYKRES3.LST), [WYKRES3.BAS](Bajtek/1988/10/WYKRES3.BAS), [WYKRES4.LST](Bajtek/1988/10/WYKRES4.LST), [WYKRES4.BAS](Bajtek/1988/10/WYKRES4.BAS), [WYKRES5.LST](Bajtek/1988/10/WYKRES5.LST), [WYKRES5.BAS](Bajtek/1988/10/WYKRES5.BAS) |
| 8 | Poprawka Turbo Basic-a | Jacek Żuk | [POPR1.LST](Bajtek/1988/10/POPR1.LST), [POPR1.BAS](Bajtek/1988/10/POPR1.BAS), [POPR2.LST](Bajtek/1988/10/POPR2.LST), [POPR2.BAS](Bajtek/1988/10/POPR2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 11

![Bajtek 1988 / 11](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_11_male.jpg)

All listings on Atari disk image: [bajtek_1988_11.atr](Bajtek/1988/11/bajtek_1988_11.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Centronics dla Atari | Marek Renner | [CENTRON.LST](Bajtek/1988/11/CENTRON.LST), [CENTRON.BAS](Bajtek/1988/11/CENTRON.BAS) |
| 9 | Zamiana napisów raz jeszcze | Mirosław Honkowicz | [NAPISY1.LST](Bajtek/1988/11/NAPISY1.LST), [NAPISY1.BAS](Bajtek/1988/11/NAPISY1.BAS), [NAPISY2.LST](Bajtek/1988/11/NAPISY2.LST), [NAPISY2.BAS](Bajtek/1988/11/NAPISY2.BAS) |
| 10 | Liczby rzeczywiste w Action! | Wojciech Zientara | [REAL.ACT](Bajtek/1988/11/REAL.ACT) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / 12

![Bajtek 1988 / 12](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1988_12_male.jpg)

All listings on Atari disk image: [bajtek_1988_12.atr](Bajtek/1988/12/bajtek_1988_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 4 | Muzyka w przerwaniach | Maciej Rempiński | [MUZYKA.LST](Bajtek/1988/12/MUZYKA.LST), [MUZYKA.BAS](Bajtek/1988/12/MUZYKA.BAS) |
| 4 | Zamalowywanie | Andrzej Postrzednik | [FILL.ACT](Bajtek/1988/12/FILL.ACT) |
| 5 | Pożyteczne procedury | Wojciech Zientara | [PROCEDUR.ACT](Bajtek/1988/12/PROCEDUR.ACT) |
| 5 | Namer | Robert Pindera | [NAMER.LST](Bajtek/1988/12/NAMER.LST), [NAMER.BAS](Bajtek/1988/12/NAMER.BAS) |
| 6 | Liczby zespolone | Dariusz Tabaczyński | [ZESPOL.LST](Bajtek/1988/12/ZESPOL.LST), [ZESPOL.BAS](Bajtek/1988/12/ZESPOL.BAS) |
| 7 | Samouruchamianie przez Enter | Roman Tomaszewski, Andrzej Zalewski | [AUTORUN1.LST](Bajtek/1988/12/AUTORUN1.LST), [AUTORUN1.BAS](Bajtek/1988/12/AUTORUN1.BAS), [AUTORUN2.LST](Bajtek/1988/12/AUTORUN2.LST), [AUTORUN2.BAS](Bajtek/1988/12/AUTORUN2.BAS) |
| 7 | Przeszukiwanie pamięci | Krzysztof Kołodziej | [PRZESZUK.LST](Bajtek/1988/12/PRZESZUK.LST), [PRZESZUK.BAS](Bajtek/1988/12/PRZESZUK.BAS) |
| 8 | Peek i Poke w Kyan Pascalu | Ryszard Wiech | [PEEKPOKE.PAS](Bajtek/1988/12/PEEKPOKE.PAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1988 / Tylko o Atari

![Bajtek 1988 / Tylko o Atari](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_Tylko_o_Atari_02_male.jpg)

All listings on Atari disk image: [bajtek_1988_tylko_o_atari.atr](Bajtek/1988/Tylko_o_Atari/bajtek_1988_tylko_o_atari.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Okienka | Andrzej Biazik | [OKIENKA1.LST](Bajtek/1988/Tylko_o_Atari/OKIENKA1.LST), [OKIENKA1.BAS](Bajtek/1988/Tylko_o_Atari/OKIENKA1.BAS), [OKIENKA2.LST](Bajtek/1988/Tylko_o_Atari/OKIENKA2.LST), [OKIENKA2.BAS](Bajtek/1988/Tylko_o_Atari/OKIENKA2.BAS) |
| 9 | Optymalizacja zapisu na dyskietce | Przemysław Strzelecki | [OPTYMAL1.LST](Bajtek/1988/Tylko_o_Atari/OPTYMAL1.LST), [OPTYMAL1.BAS](Bajtek/1988/Tylko_o_Atari/OPTYMAL1.BAS), [OPTYMAL2.LST](Bajtek/1988/Tylko_o_Atari/OPTYMAL2.LST), [OPTYMAL2.BAS](Bajtek/1988/Tylko_o_Atari/OPTYMAL2.BAS) |
| 10 | Buldoger Copy | Grzegorze Mikrut | [BULDCOPY.LST](Bajtek/1988/Tylko_o_Atari/BULDCOPY.LST), [BULDCOPY.BAS](Bajtek/1988/Tylko_o_Atari/BULDCOPY.BAS) |
| 22 | Pchełka | Jakub Cebula | [PCHLA.LST](Bajtek/1988/Tylko_o_Atari/PCHLA.LST), [PCHLA.BAS](Bajtek/1988/Tylko_o_Atari/PCHLA.BAS) |
| 25 | Ocena - Matematyka | Rafał Janiak, Wojciech Zientara | [OCENA1.LST](Bajtek/1988/Tylko_o_Atari/OCENA1.LST), [OCENA1.BAS](Bajtek/1988/Tylko_o_Atari/OCENA1.BAS), [OCENA2.LST](Bajtek/1988/Tylko_o_Atari/OCENA2.LST), [OCENA2.BAS](Bajtek/1988/Tylko_o_Atari/OCENA2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 1

![Bajtek 1989 / 1](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_01_male.jpg)

All listings on Atari disk image: [bajtek_1989_1.atr](Bajtek/1989/1/bajtek_1989_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Edytor BASICA | | [EDYTBAS.LST](Bajtek/1989/1/EDYTBAS.LST), [EDYTBAS.BAS](Bajtek/1989/1/EDYTBAS.BAS) |
| 6 | Walka powietrzna | Wojciech Zientara | [WALKA.LST](Bajtek/1989/1/WALKA.LST), [WALKA.BAS](Bajtek/1989/1/WALKA.BAS) |
| 7 | Polskie litery w "ROM" | Sebastian Siwy | [POLSKIE.LST](Bajtek/1989/1/POLSKIE.LST), [POLSKIE.BAS](Bajtek/1989/1/POLSKIE.BAS) |
| 8 | Funkcje SIN, COS i SQR w Action! | Andrzej Postrzednik | [FUNKCJE1.LST](Bajtek/1989/1/FUNKCJE1.LST), [FUNKCJE1.BAS](Bajtek/1989/1/FUNKCJE1.BAS), [FUNKCJE2.ACT](Bajtek/1989/1/FUNKCJE2.ACT), [FUNKCJE3.ACT](Bajtek/1989/1/FUNKCJE3.ACT), [FUNKCJE4.ACT](Bajtek/1989/1/FUNKCJE4.ACT), [FUNKCJ5A.ACT](Bajtek/1989/1/FUNKCJ5A.ACT), [FUNKCJ5B.ACT](Bajtek/1989/1/FUNKCJ5B.ACT), [FUNKCJ5C.ACT](Bajtek/1989/1/FUNKCJ5C.ACT), [FUNKCJ5D.ACT](Bajtek/1989/1/FUNKCJ5D.ACT), [FUNKCJ5E.ACT](Bajtek/1989/1/FUNKCJ5E.ACT), [FUNKCJE26.ACT](Bajtek/1989/1/FUNKCJE6.ACT), [FUNKCJE7.ACT](Bajtek/1989/1/FUNKCJE7.ACT) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 2

![Bajtek 1989 / 2](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_02_male.jpg)

All listings on Atari disk image: [bajtek_1989_2.atr](Bajtek/1989/2/bajtek_1989_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 8 | Projektowanie znaków | Leszek Taratura | [ZNAKI.LST](Bajtek/1989/2/ZNAKI.LST), [ZNAKI.BAS](Bajtek/1989/2/ZNAKI.BAS) |
| 9 | Klawisze konsoli | Piotr Musiatowicz | [KONSOLA.LST](Bajtek/1989/2/KONSOLA.LST), [KONSOLA.BAS](Bajtek/1989/2/KONSOLA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 4

![Bajtek 1989 / 4](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_04_male.jpg)

All listings on Atari disk image: [bajtek_1989_4.atr](Bajtek/1989/4/bajtek_1989_4.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Input/Output w Action! | Wojciech Zientara | [INOUT.ACT](Bajtek/1989/4/INOUT.ACT) |
| 7 | Katalog dyskietki | Kasia Lekawska | [DIR.LST](Bajtek/1989/4/DIR.LST), [DIR.BAS](Bajtek/1989/4/DIR.BAS) |
| 7 | Przewijanie napisu | Jakub Gawlikowski | [PRZEWIJA.LST](Bajtek/1989/4/PRZEWIJA.LST), [PRZEWIJA.BAS](Bajtek/1989/4/PRZEWIJA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 5

![Bajtek 1989 / 5](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_05_male.jpg)

All listings on Atari disk image: [bajtek_1989_5.atr](Bajtek/1989/5/bajtek_1989_5.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Dodatkowe klawisze inaczej | Wojciech Zientara | [KLAWISZE.LST](Bajtek/1989/5/KLAWISZE.LST), [KLAWISZE.BAS](Bajtek/1989/5/KLAWISZE.BAS) |
| 7 | Kilka uwag o stosowaniu systemu Kyan Pascal na Atari | Ryszard Wiech | [PROCEDUR.PAS](Bajtek/1989/5/PROCEDUR.PAS) |
| 8 | Powierzchnia obszaru nieregularnego | Wojciech Zientara | [POLE.LST](Bajtek/1989/5/POLE.LST), [POLE.BAS](Bajtek/1989/5/POLE.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 6

![Bajtek 1989 / 6](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_06_male.jpg)

All listings on Atari disk image: [bajtek_1989_6.atr](Bajtek/1989/6/bajtek_1989_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Dodatkowe klawisze inaczej | Wojciech Zientara | [KLAWISZE.LST](Bajtek/1989/6/KLAWISZE.LST), [KLAWISZE.BAS](Bajtek/1989/6/KLAWISZE.BAS) |
| 7 | Liczby e i π | Wojciech Przybył | [E.LST](Bajtek/1989/6/E.LST), [E.BAS](Bajtek/1989/6/E.BAS), [PI.LST](Bajtek/1989/6/PI.LST), [PI.BAS](Bajtek/1989/6/PI.BAS) |
| 8 | Daleko w kosmosie | Wojciech Zientara | [DALEKO.LST](Bajtek/1989/6/DALEKO.LST), [DALEKO.BAS](Bajtek/1989/6/DALEKO.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 7

![Bajtek 1989 / 7](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_07_male.jpg)

All listings on Atari disk image: [bajtek_1989_7.atr](Bajtek/1989/7/bajtek_1989_7.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 7 | Duszki w Action! | Wojciech Zientara | [DUSZKI1.ACT](Bajtek/1989/7/DUSZKI1.ACT), [DUSZKI2.ACT](Bajtek/1989/7/DUSZKI2.ACT) |
| 8 | Rockford dla każdego | Zdenêk Rubý | [ROCKFORD.LST](Bajtek/1989/7/ROCKFORD.LST), [ROCKFORD.BAS](Bajtek/1989/7/ROCKFORD.BAS) |
| 9 | Obrazki w DATA | Wojciech Zientara | [GRAF1.LST](Bajtek/1989/7/GRAF1.LST), [GRAF1.BAS](Bajtek/1989/7/GRAF1.BAS), [GRAF124.LST](Bajtek/1989/7/GRAF124.LST), [GRAF124.BAS](Bajtek/1989/7/GRAF124.BAS), [GRAF3.LST](Bajtek/1989/7/GRAF3.LST), [GRAF3.BAS](Bajtek/1989/7/GRAF3.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 8

![Bajtek 1989 / 8](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_08_male.jpg)

All listings on Atari disk image: [bajtek_1989_8.atr](Bajtek/1989/8/bajtek_1989_8.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Przesuw pionowy | Wojciech Zientara | [PRZESUW.LST](Bajtek/1989/8/PRZESUW.LST), [PRZESUW.BAS](Bajtek/1989/8/PRZESUW.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 9

![Bajtek 1989 / 9](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_09_male.jpg)

All listings on Atari disk image: [bajtek_1989_9.atr](Bajtek/1989/9/bajtek_1989_9.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Chaos loader | Janusz Pelc | [LOADER.LST](Bajtek/1989/9/LOADER.LST), [LOADER.BAS](Bajtek/1989/9/LOADER.BAS) |
| 7 | Weryfikator | Andrzej Zalewski | [WERYFIK1.LST](Bajtek/1989/9/WERYFIK1.LST), [WERYFIK1.BAS](Bajtek/1989/9/WERYFIK1.BAS), [WERYFIK2.LST](Bajtek/1989/9/WERYFIK2.LST), [WERYFIK2.BAS](Bajtek/1989/9/WERYFIK2.BAS) |
| 7 | Odzyskiwanie programów | Artur Lisowski | [ODZYSK.LST](Bajtek/1989/9/ODZYSK.LST), [ODZYSK.BAS](Bajtek/1989/9/ODZYSK.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 10

![Bajtek 1989 / 10](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_10_male.jpg)

All listings on Atari disk images: [bajtek_1989_10a.atr](Bajtek/1989/10/bajtek_1989_10a.atr), [bajtek_1989_10b.atr](Bajtek/1989/10/bajtek_1989_10b.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 8 | Sito Eratostenesa | Mieczysław Gabryczewski | [PRIME.ACT](Bajtek/1989/10/PRIME.ACT) |
| 9 | Multi-kopier | Andrzej Zalewski | [KOPIER.LST](Bajtek/1989/10/KOPIER.LST), [KOPIER.BAS](Bajtek/1989/10/KOPIER.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 11

![Bajtek 1989 / 11](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_11_male.jpg)

All listings on Atari disk image: [bajtek_1989_11.atr](Bajtek/1989/11/bajtek_1989_11.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 6 | Hardcopy | Paweł Miasojedow | [HARDCOPY.LST](Bajtek/1989/11/HARDCOPY.LST), [HARDCOPY.BAS](Bajtek/1989/11/HARDCOPY.BAS) |
| 9 | Turbo Freezer Killer | (wist) | [TFKILLER.LST](Bajtek/1989/11/TFKILLER.LST), [TFKILLER.BAS](Bajtek/1989/11/TFKILLER.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / 12

![Bajtek 1989 / 12](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1989_12_male.jpg)

All listings on Atari disk image: [bajtek_1989_12.atr](Bajtek/1989/12/bajtek_1989_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 7 | Mini Tetris | Tomasz Konatkowski | [TETRIS.ACT](Bajtek/1989/12/TETRIS.ACT) |
| 8 | Syntezator | Piotr Bendyk, Wojtek | [SYNTEZ1.LST](Bajtek/1989/12/SYNTEZ1.LST), [SYNTEZ1.BAS](Bajtek/1989/12/SYNTEZ1.BAS), [SYNTEZ2.LST](Bajtek/1989/12/SYNTEZ2.LST), [SYNTEZ2.BAS](Bajtek/1989/12/SYNTEZ2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1989 / Tylko dla początkujących

![Bajtek 1989 / Tylko dla początkujacych](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_Tylko_dla_poczatkujacych_male.jpg)

All listings on Atari disk image: [bajtek_1989_tylko_dla_poczatkujacych.atr](Bajtek/1989/Tylko_dla_poczatkujacych/bajtek_1989_tylko_dla_poczatkujacych.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 20 | Polskie litery | Wojciech Zientara | [POLSKIE.LST](Bajtek/1989/Tylko_dla_poczatkujacych/POLSKIE.LST), [POLSKIE.BAS](Bajtek/1989/Tylko_dla_poczatkujacych/POLSKIE.BAS) |
| 26 | Wiosełka | | [PADDLES.LST](Bajtek/1989/Tylko_dla_poczatkujacych/PADDLES.LST), [PADDLES.BAS](Bajtek/1989/Tylko_dla_poczatkujacych/PADDLES.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1990 / 1-2

![Bajtek 1990 / 1-2](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1990_01_02_male.jpg)

All listings on Atari disk image: [bajtek_1990_1_2.atr](Bajtek/1990/1-2/bajtek_1990_1_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Magazynier | Wojciech Zientara | [MAGAZYN.LST](Bajtek/1990/1-2/MAGAZYN.LST), [MAGAZYN.BAS](Bajtek/1990/1-2/MAGAZYN.BAS) |
| 6 | Edytor Basica | (wz) | [EDYTBAS.LST](Bajtek/1990/1-2/EDYTBAS.LST), [EDYTBAS.BAS](Bajtek/1990/1-2/EDYTBAS.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1990 / 3-4

![Bajtek 1990 / 3-4](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1990_03_04_male.jpg)

All listings on Atari disk image: [bajtek_1990_3_4.atr](Bajtek/1990/3-4/bajtek_1990_3_4.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 9 | Okręty | Leszek Stróżowski | [OKRETY.LST](Bajtek/1990/3-4/OKRETY.LST), [OKRETY.BAS](Bajtek/1990/3-4/OKRETY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1990 / 5-6

![Bajtek 1990 / 5-6](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1990_05_06_male.jpg)

All listings on Atari disk image: [bajtek_1990_5_6.atr](Bajtek/1990/5-6/bajtek_1990_5_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 8 | Graficzny listing | Jacek Żuk, Tomasz Żuk | [GRAF1I2.LST](Bajtek/1990/5-6/GRAF1I2.LST), [GRAF1I2.BAS](Bajtek/1990/5-6/GRAF1I2.BAS), [GRAF1I3.LST](Bajtek/1990/5-6/GRAF1I3.LST), [GRAF1I3.BAS](Bajtek/1990/5-6/GRAF1I3.BAS), [GRAF1I4.LST](Bajtek/1990/5-6/GRAF1I4.LST), [GRAF1I4.BAS](Bajtek/1990/5-6/GRAF1I4.BAS) |

---
### Bajtek 1990 / 7-8

![Bajtek 1990 / 7-8](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1990_07_08_male.jpg)

All listings on Atari disk image: [bajtek_1990_7_8.atr](Bajtek/1990/7-8/bajtek_1990_7_8.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 13 | Action! Zapis i odczyt obszaru pamięci | Andrzej Postrzednik | [SAVELOAD.ACT](Bajtek/1990/7-8/SAVELOAD.ACT) |
| 13 | Obrońca Ziemi | Piotr Karpiuk | [OBRONCA.LST](Bajtek/1990/7-8/OBRONCA.LST), [OBRONCA.BAS](Bajtek/1990/7-8/OBRONCA.BAS) |
| 14 | Trójwymiarowe wykresy | Andrzej Orłowski | [WYKRES1.LST](Bajtek/1990/7-8/WYKRES1.LST), [WYKRES1.BAS](Bajtek/1990/7-8/WYKRES1.BAS), [WYKRES2.LST](Bajtek/1990/7-8/WYKRES2.LST), [WYKRES2.BAS](Bajtek/1990/7-8/WYKRES2.BAS), [WYKRES3.LST](Bajtek/1990/7-8/WYKRES3.LST), [WYKRES3.BAS](Bajtek/1990/7-8/WYKRES3.BAS), [WYKRES4.LST](Bajtek/1990/7-8/WYKRES4.LST), [WYKRES4.BAS](Bajtek/1990/7-8/WYKRES4.BAS), [WYKRES5.LST](Bajtek/1990/7-8/WYKRES5.LST), [WYKRES5.BAS](Bajtek/1990/7-8/WYKRES5.BAS), [WYKRES6.LST](Bajtek/1990/7-8/WYKRES6.LST), [WYKRES6.BAS](Bajtek/1990/7-8/WYKRES6.BAS) |
| 15 | 80 znaków w wierszu | Michał Skowroński | [80ZNAK1.LST](Bajtek/1990/7-8/80ZNAK1.LST), [80ZNAK1.BAS](Bajtek/1990/7-8/80ZNAK1.BAS), [80ZNAK2.LST](Bajtek/1990/7-8/80ZNAK2.LST), [80ZNAK2.BAS](Bajtek/1990/7-8/80ZNAK2.BAS) |
| 16 | Kasetowy Ramdysk | Sebastian Siwy, H. Krasucki | [RAMDYSK.LST](Bajtek/1990/7-8/RAMDYSK.LST), [RAMDYSK.BAS](Bajtek/1990/7-8/RAMDYSK.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1990 / 9-10

![Bajtek 1990 / 9-10](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1990_09_10_male.jpg)

All listings on Atari disk image: [bajtek_1990_9_10.atr](Bajtek/1990/9-10/bajtek_1990_9_10.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 12 | Mini edytor duszków | Michał Widera | [DUSZKI.LST](Bajtek/1990/9-10/DUSZKI.LST), [DUSZKI.BAS](Bajtek/1990/9-10/DUSZKI.BAS) |
| 13 | Toto-lotek | Leszek Stróżowski | [LOTEK.LST](Bajtek/1990/9-10/LOTEK.LST), [LOTEK.BAS](Bajtek/1990/9-10/LOTEK.BAS) |
| 13 | Polskie litery w Action! | Grzegorz Sarnecki | [POLSKIE.ACT](Bajtek/1990/9-10/POLSKIE.ACT), [OKNO1.ACT](Bajtek/1990/9-10/OKNO1.ACT), [OKNO2.ACT](Bajtek/1990/9-10/OKNO2.ACT) |
| 15 | Podprogramy Adres ora DIR w Kyan Pascalu | Ryszard Wiech | [ADRES.PAS](Bajtek/1990/9-10/ADRES.PAS), [DIR.PAS](Bajtek/1990/9-10/DIR.PAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1990 / 11-12

![Bajtek 1990 / 11-12](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1990_11_12_male.jpg)

All listings on Atari disk image: [bajtek_1990_11_12.atr](Bajtek/1990/11-12/bajtek_1990_11_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 8 | Przerwania w Action! | Andrzej Postrzednik | [PRZERW.ACT](Bajtek/1990/11-12/PRZERW.ACT) |
| 9 | Jeszcze raz o sortowaniu | Petr Válka | [SORT.LST](Bajtek/1990/11-12/SORT.LST), [SORT.BAS](Bajtek/1990/11-12/SORT.BAS), [SORTTEMP.LST](Bajtek/1990/11-12/SORTTEMP.LST), [SORTTEMP.BAS](Bajtek/1990/11-12/SORTTEMP.BAS) |
| 10 | Piękno matematyki | Jacek Pliszczyński | [PIEKNO1.LST](Bajtek/1990/11-12/PIEKNO1.LST), [PIEKNO1.BAS](Bajtek/1990/11-12/PIEKNO1.BAS), [PIEKNO2.LST](Bajtek/1990/11-12/PIEKNO2.LST), [PIEKNO2.BAS](Bajtek/1990/11-12/PIEKNO2.BAS) |
| 11 | Licznik czasu | Michał Wilga | [LICZNIK.LST](Bajtek/1990/11-12/LICZNIK.LST), [LICZNIK.BAS](Bajtek/1990/11-12/LICZNIK.BAS) |
| 11 | Zapis i odczyt rysunków Rambrandta | Sebastian Siwy | [RAMBRAND.LST](Bajtek/1990/11-12/RAMBRAND.LST), [RAMBRAND.BAS](Bajtek/1990/11-12/RAMBRAND.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 1

![Bajtek 1991 / 1](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_01_male.jpg)

All listings on Atari disk image: [bajtek_1991_1.atr](Bajtek/1991/1/bajtek_1991_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 11 | Instrukcja INPUT inaczej | Andrzej Zieliński | [INPUT.LST](Bajtek/1991/1/INPUT.LST), [INPUT.BAS](Bajtek/1991/1/INPUT.BAS) |
| 11 | Migający kursor | Andrzej Zieliński | [KURSOR.LST](Bajtek/1991/1/KURSOR.LST), [KURSOR.BAS](Bajtek/1991/1/KURSOR.BAS) |
| 12 | Player Graphics | Michał Skowroński | [PLAYERGR.LST](Bajtek/1991/1/PLAYERGR.LST), [PLAYERGR.BAS](Bajtek/1991/1/PLAYERGR.BAS), [ARKANOID.LST](Bajtek/1991/1/ARKANOID.LST), [ARKANOID.BAS](Bajtek/1991/1/ARKANOID.BAS) |
| 13 | Nowe wykorzystanie Sita Eratostenesa | Wojciech Przybył | [SITO1.LST](Bajtek/1991/1/SITO1.LST), [SITO1.BAS](Bajtek/1991/1/SITO1.BAS), [SITO2.LST](Bajtek/1991/1/SITO2.LST), [SITO2.BAS](Bajtek/1991/1/SITO2.BAS) |
| 13 | Funkcje kątów wielokrotnych | Arnold Adamczyk | [FUNKCJE.LST](Bajtek/1991/1/FUNKCJE.LST), [FUNKCJE.BAS](Bajtek/1991/1/FUNKCJE.BAS) |
| 13 | Kolory w DLI | Sławomir Słomiński | [KOLORY.LST](Bajtek/1991/1/KOLORY.LST), [KOLORY.BAS](Bajtek/1991/1/KOLORY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 2

![Bajtek 1991 / 2](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_02_male.jpg)

All listings on Atari disk image: [bajtek_1991_2.atr](Bajtek/1991/2/bajtek_1991_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 11 | Bzzzzzz...... | Marek Frueauff | [BZZZZZ.LST](Bajtek/1991/2/BZZZZZ.LST), [BZZZZZ.BAS](Bajtek/1991/2/BZZZZZ.BAS) |
| 12 | Permutacje | Marcin Brzeziński | [PERMUT1.LST](Bajtek/1991/2/PERMUT1.LST), [PERMUT1.BAS](Bajtek/1991/2/PERMUT1.BAS), [PERMUT2.LST](Bajtek/1991/2/PERMUT2.LST), [PERMUT2.BAS](Bajtek/1991/2/PERMUT2.BAS) |
| 14 | Dwumian Newtona | Wojciech Przybył | [DWUMIAN.LST](Bajtek/1991/2/DWUMIAN.LST), [DWUMIAN.BAS](Bajtek/1991/2/DWUMIAN.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 3

![Bajtek 1991 / 3](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_03_male.jpg)

All listings on Atari disk image: [bajtek_1991_3.atr](Bajtek/1991/3/bajtek_1991_3.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 14 | Matematyka i sztuka | Leszek Taratura | [MATEMAT.LST](Bajtek/1991/3/MATEMAT.LST), [MATEMAT.BAS](Bajtek/1991/3/MATEMAT.BAS) |
| 14 | Konfiguracja | Łukasz Komsta | [KONFIG.ACT](Bajtek/1991/3/KONFIG.ACT) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 4

![Bajtek 1991 / 4](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_04_male.jpg)

All listings on Atari disk image: [bajtek_1991_4.atr](Bajtek/1991/4/bajtek_1991_4.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 26 | Kursor-duszek | Leszek Taratura | [KURSOR.LST](Bajtek/1991/4/KURSOR.LST), [KURSOR.BAS](Bajtek/1991/4/KURSOR.BAS) |
| 27 | Pchełki graficzne | Jacek Pliszczyński | [PCHELKA1.LST](Bajtek/1991/4/PCHELKA1.LST), [PCHELKA1.BAS](Bajtek/1991/4/PCHELKA1.BAS), [PCHELKA2.LST](Bajtek/1991/4/PCHELKA2.LST), [PCHELKA2.BAS](Bajtek/1991/4/PCHELKA2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 5

![Bajtek 1991 / 5](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_05_male.jpg)

All listings on Atari disk image: [bajtek_1991_5.atr](Bajtek/1991/5/bajtek_1991_5.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 15 | Ruchomy krajobraz | Piotr Waśkiewicz | [KRAJOBR1.LST](Bajtek/1991/5/KRAJOBR1.LST), [KRAJOBR1.BAS](Bajtek/1991/5/KRAJOBR1.BAS), [KRAJOBR2.LST](Bajtek/1991/5/KRAJOBR2.LST), [KRAJOBR2.BAS](Bajtek/1991/5/KRAJOBR2.BAS) |
| 15 | Pauza | Grzegorz Sarnecki | [PAUZA.LST](Bajtek/1991/5/PAUZA.LST), [PAUZA.BAS](Bajtek/1991/5/PAUZA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 6

![Bajtek 1991 / 6](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_06_male.jpg)

All listings on Atari disk image: [bajtek_1991_6.atr](Bajtek/1991/6/bajtek_1991_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 20 | Trójwymiarowe wykresy | Piotr Waśkiewicz | [WYKRESY1.LST](Bajtek/1991/6/WYKRESY1.LST), [WYKRESY1.BAS](Bajtek/1991/6/WYKRESY1.BAS), [WYKRESY2.LST](Bajtek/1991/6/WYKRESY2.LST), [WYKRESY2.BAS](Bajtek/1991/6/WYKRESY2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 7

![Bajtek 1991 / 7](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_07_male.jpg)

All listings on Atari disk image: [bajtek_1991_7.atr](Bajtek/1991/7/bajtek_1991_7.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 10 | Rozszerzamy bibliotekę Action! | Łukasz Komsta | [PROCEDUR.ACT](Bajtek/1991/7/PROCEDUR.ACT) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 8

![Bajtek 1991 / 8](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_08_male.jpg)

All listings on Atari disk image: [bajtek_1991_8.atr](Bajtek/1991/8/bajtek_1991_8.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 19 | Basic bootstrap | Krzysztof Klimczak | [AUTORUN.LST](Bajtek/1991/8/AUTORUN.LST), [AUTORUN.BAS](Bajtek/1991/8/AUTORUN.BAS), [GENERAT.LST](Bajtek/1991/8/GENERAT.LST), [GENERAT.BAS](Bajtek/1991/8/GENERAT.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 9

![Bajtek 1991 / 9](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_09_male.jpg)

All listings on Atari disk image: [bajtek_1991_9.atr](Bajtek/1991/9/bajtek_1991_9.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 13 | Tails maker | Piotr Miller | [TAILS.LST](Bajtek/1991/9/TAILS.LST), [TAILS.BAS](Bajtek/1991/9/TAILS.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 10

![Bajtek 1991 / 10](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_10_male.jpg)

All listings on Atari disk image: [bajtek_1991_10.atr](Bajtek/1991/10/bajtek_1991_10.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 15 | Modyfikacja "Edytora BASIC-a" | Witold Rybiński | [EDYTBAS.LST](Bajtek/1991/10/EDYTBAS.LST), [EDYTBAS.BAS](Bajtek/1991/10/EDYTBAS.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1991 / 12

![Bajtek 1991 / 12](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1991_12_male.jpg)

All listings on Atari disk image: [bajtek_1991_12.atr](Bajtek/1991/12/bajtek_1991_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 17 | Wykresy dowolnych funkcji | Piotr Karpiuk | [WYKRESY.LST](Bajtek/1991/12/WYKRESY.LST), [WYKRESY.BAS](Bajtek/1991/12/WYKRESY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1992 / 1

![Bajtek 1992 / 1](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1992_01_male.jpg)

All listings on Atari disk image: [bajtek_1992_01.atr](Bajtek/1992/1/bajtek_1992_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 13 | Mini COPY | Robert Kleniewski | [MINICOPY.LST](Bajtek/1992/1/MINICOPY.LST), [MINICOPY.BAS](Bajtek/1992/1/MINICOPY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1992 / 5

![Bajtek 1992 / 5](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1992_05_male.jpg)

All listings on Atari disk image: [bajtek_1992_05.atr](Bajtek/1992/5/bajtek_1992_5.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 12 | Rozszerzenie w ATARI XL/XE | Tomasz S. Piotrowski | [TEST256.LST](Bajtek/1992/5/TEST256.LST), [TEST256.BAS](Bajtek/1992/5/TEST256.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1992 / 6

![Bajtek 1992 / 6](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1992_06_male.jpg)

All listings on Atari disk image: [bajtek_1992_06.atr](Bajtek/1992/6/bajtek_1992_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 8 | Rozszerzenie pamięci RAM w komputerach ATARI XL/XE | Tomasz S. Piotrowski | [TEST320.LST](Bajtek/1992/6/TEST320.LST), [TEST320.BAS](Bajtek/1992/6/TEST320.BAS) |
| 10 | Formater tekstów dla Atari XL/XE | Tomasz S. Piotrowski | [FORMATER.LST](Bajtek/1992/6/FORMATER.LST), [FORMATER.BAS](Bajtek/1992/6/FORMATER.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1992 / 8

![Bajtek 1992 / 8](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1992_08_male.jpg)

All listings on Atari disk image: [bajtek_1992_08.atr](Bajtek/1992/8/bajtek_1992_8.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 10 | Zalety trybu tekstowego | Andrzej Zalewski | [TRYBY2.LST](Bajtek/1992/8/TRYBY2.LST), [TRYBY2.BAS](Bajtek/1992/8/TRYBY2.BAS), [TRYBY4.LST](Bajtek/1992/8/TRYBY4.LST), [TRYBY4.BAS](Bajtek/1992/8/TRYBY4.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1992 / 11

![Bajtek 1992 / 11](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1992_11_male.jpg)

All listings on Atari disk image: [bajtek_1992_11.atr](Bajtek/1992/11/bajtek_1992_11.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 14 | Atari XL/XE i drukarka laserowa | Tomasz S. Piotrowski | [LASER1.LST](Bajtek/1992/11/LASER1.LST), [LASER1.BAS](Bajtek/1992/11/LASER1.BAS), [LASER2.LST](Bajtek/1992/11/LASER2.LST), [LASER2.BAS](Bajtek/1992/11/LASER2.BAS), [LASER3.LST](Bajtek/1992/11/LASER3.LST), [LASER3.BAS](Bajtek/1992/11/LASER3.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1993 / 3

![Bajtek 1993 / 3](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1993_03_male.jpg)

All listings on Atari disk image: [bajtek_1993_3.atr](Bajtek/1993/3/bajtek_1993_3.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 17 | Kopiowanie dysk-kaseta cz.II | Jan Bieńkowski | [BCOPY.LST](Bajtek/1993/3/BCOPY.LST), [BCOPY.BAS](Bajtek/1993/3/BCOPY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1993 / 4

![Bajtek 1993 / 4](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1993_04_male.jpg)

All listings on Atari disk image: [bajtek_1993_4.atr](Bajtek/1993/4/bajtek_1993_4.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 19 | Druk grafiki | Krzysztof Klimczak | [DRUK1.LST](Bajtek/1993/4/DRUK1.LST), [DRUK1.BAS](Bajtek/1993/4/DRUK1.BAS), [DRUK2.LST](Bajtek/1993/4/DRUK2.LST), [DRUK2.BAS](Bajtek/1993/4/DRUK2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1993 / 5

![Bajtek 1993 / 5](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1993_05_male.jpg)

All listings on Atari disk image: [bajtek_1993_5.atr](Bajtek/1993/5/bajtek_1993_5.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 18 | Display List | Piotr Karkuciński | [DLIST.LST](Bajtek/1993/5/DLIST.LST), [DLIST.BAS](Bajtek/1993/5/DLIST.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1993 / 6

![Bajtek 1993 / 6](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1993_06_male.jpg)

All listings on Atari disk image: [bajtek_1993_6.atr](Bajtek/1993/6/bajtek_1993_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 19 | Display List cz.2 | Piotr Karkuciński | [DLIST1.LST](Bajtek/1993/6/DLIST1.LST), [DLIST1.BAS](Bajtek/1993/6/DLIST1.BAS), [DLIST1I2.LST](Bajtek/1993/6/DLIST1I2.LST), [DLIST1I2.BAS](Bajtek/1993/6/DLIST1I2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1993 / 7

![Bajtek 1993 / 7](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1993_07_male.jpg)

All listings on Atari disk image: [bajtek_1993_7.atr](Bajtek/1993/7/bajtek_1993_7.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 17 | Poznaj swój komputer | Konrad Kokoszkiewicz | [ADRES.LST](Bajtek/1993/7/ADRES.LST), [ADRES.BAS](Bajtek/1993/7/ADRES.BAS) |
| 17 | Mała rzecz a cieszy | Konrad Kokoszkiewicz | [FULLSCR.LST](Bajtek/1993/7/FULLSCR.LST), [FULLSCR.BAS](Bajtek/1993/7/FULLSCR.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1993 / 8-9

![Bajtek 1993 / 8-9](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1993_08_09_male.jpg)

All listings on Atari disk image: [bajtek_1993_8_9.atr](Bajtek/1993/8-9/bajtek_1993_8_9.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 15 | Łamigłówka | Piotr Karkuciński | [LAMIGLOW.LST](Bajtek/1993/8-9/LAMIGLOW.LST), [LAMIGLOW.BAS](Bajtek/1993/8-9/LAMIGLOW.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1993 / 10

![Bajtek 1993 / 10](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1993_10_male.jpg)

All listings on Atari disk image: [bajtek_1993_10.atr](Bajtek/1993/10/bajtek_1993_10.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 20 | Pisanie na jednym ekranie | Piotr Karkuciński | [EDYTOR.LST](Bajtek/1993/10/EDYTOR.LST), [EDYTOR.BAS](Bajtek/1993/10/EDYTOR.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1993 / 12

![Bajtek 1993 / 12](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1993_12_male.jpg)

All listings on Atari disk image: [bajtek_1993_12.atr](Bajtek/1993/12/bajtek_1993_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 51 | Zegar dla Atari XL/XE | Rafał Piasek | [ZEGAR.LST](Bajtek/1993/12/ZEGAR.LST), [ZEGAR.BAS](Bajtek/1993/12/ZEGAR.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1994 / 2

![Bajtek 1994 / 2](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1994_02_male.jpg)

All listings on Atari disk image: [bajtek_1994_2.atr](Bajtek/1994/2/bajtek_1994_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 55 | Telefoniczna baza danych | Maciej Wiewiórski | [BAZA.LST](Bajtek/1994/2/BAZA.LST), [BAZA.BAS](Bajtek/1994/2/BAZA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Bajtek 1995 / 3

![Bajtek 1995 / 3](http://atarionline.pl/biblioteka/czasopisma/Bajtek/Bajtek_1995_03_male.jpg)

All listings on Atari disk image: [bajtek_1995_3.atr](Bajtek/1995/3/bajtek_1995_3.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 38 | Przenoszenie obrazów pomiędzy Atari XL/XE i IBM PC | Mirosław Sobczak | [BMPMIC.LST](Bajtek/1995/3/BMPMIC.LST), [BMPMIC.BAS](Bajtek/1995/3/BMPMIC.BAS) |

[Go to Table of contents](#table-of-contents)

## Komputer

All scanned issues of **Komputer** magazine are available on [atarionline.pl](http://atarionline.pl/v01/index.php?subaction=showfull&id=1234027498&archive=&start_from=0&ucat=8&ct=biblioteka#zin=Komputer__rok=Wszystkie)

### Komputer 1986 / 8

![Komputer 1986 / 8](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1986_08_male.jpg)

All listings on Atari disk image: [komputer_1986_8.atr](Komputer/1986/8/komputer_1986_8.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 14 | Atari Display List | Krzysztof Bednarek | [DLIST.LST](Komputer/1986/8/DLIST.LST), [DLIST.BAS](Komputer/1986/8/DLIST.BAS), [DRGANIA.LST](Komputer/1986/8/DRGANIA.LST), [DRGANIA.BAS](Komputer/1986/8/DRGANIA.BAS), [VSCROLL.LST](Komputer/1986/8/VSCROLL.LST), [VSCROLL.BAS](Komputer/1986/8/VSCROLL.BAS), [PLYNNY.LST](Komputer/1986/8/PLYNNY.LST), [PLYNNY.BAS](Komputer/1986/8/PLYNNY.BAS), [HSCROLL.LST](Komputer/1986/8/HSCROLL.LST), [HSCROLL.BAS](Komputer/1986/8/HSCROLL.BAS) |
| 18 | Generator polskich liter na Atari 800 XL/XE | Wojciech Zientara | [POLSKIE.LST](Komputer/1986/8/POLSKIE.LST), [POLSKIE.BAS](Komputer/1986/8/POLSKIE.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1986 / 9

![Komputer 1986 / 9](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1986_09_male.jpg)

All listings on Atari disk image: [komputer_1986_9.atr](Komputer/1986/9/komputer_1986_9.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 13 | Komputerowy Master Mind | Wojciech Jedliczka | [CBREAKER.LST](Komputer/1986/9/CBREAKER.LST), [CBREAKER.BAS](Komputer/1986/9/CBREAKER.BAS) |
| 18 | Pomóc Dżinowi | Ryszard Sobkowski | [CZAS.LST](Komputer/1986/9/CZAS.LST), [CZAS.BAS](Komputer/1986/9/CZAS.BAS), [OKRAG.LST](Komputer/1986/9/OKRAG.LST), [OKRAG.BAS](Komputer/1986/9/OKRAG.BAS), [OKRAG2.LST](Komputer/1986/9/OKRAG2.LST), [OKRAG2.BAS](Komputer/1986/9/OKRAG2.BAS), [OKRAG3.LST](Komputer/1986/9/OKRAG3.LST), [OKRAG3.BAS](Komputer/1986/9/OKRAG3.BAS), [OKRAG4.LST](Komputer/1986/9/OKRAG4.LST), [OKRAG4.BAS](Komputer/1986/9/OKRAG4.BAS), [OKRAG5.LST](Komputer/1986/9/OKRAG5.LST), [OKRAG5.BAS](Komputer/1986/9/OKRAG5.BAS), [OKRAG6.LST](Komputer/1986/9/OKRAG6.LST), [OKRAG6.BAS](Komputer/1986/9/OKRAG6.BAS), |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1987 / 2

![Komputer 1987 / 2](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1987_02_male.jpg)

All listings on Atari disk image: [komputer_1987_2.atr](Komputer/1987/2/komputer_1987_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 33 | Pomagajmy sobie | Wojciech Jedliczka | [DATA.LST](Komputer/1987/2/DATA.LST), [DATA.BAS](Komputer/1987/2/DATA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1987 / 11

![Komputer 1987 / 11](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1987_11_male.jpg)

All listings on Atari disk image: [komputer_1987_11.atr](Komputer/1987/11/komputer_1987_11.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 48 | Nowe wzorce liter | Tomasz Puchalski | [POLSKIE.LST](Komputer/1987/11/POLSKIE.LST), [POLSKIE.BAS](Komputer/1987/11/POLSKIE.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1987 / 12

![Komputer 1987 / 12](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1987_12_male.jpg)

All listings on Atari disk image: [komputer_1987_12.atr](Komputer/1987/12/komputer_1987_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 52 | Multicolor | Sergiusz Muszalski | [MULTI.LST](Komputer/1987/12/MULTI.LST), [MULTI.BAS](Komputer/1987/12/MULTI.BAS) |
| 52 | Graf-demo | Tomasz Marszałek | [GRAFDEMO.LST](Komputer/1987/12/GRAFDEMO.LST), [GRAFDEMO.BAS](Komputer/1987/12/GRAFDEMO.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 1

![Komputer 1988 / 1](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_01_male.jpg)

All listings on Atari disk image: [komputer_1988_1.atr](Komputer/1988/1/komputer_1988_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 21 | Mikroprogramy dla Atari XL/XE | Mirosław Matlęga, Henryk Nowak | [KURSOR.LST](Komputer/1988/1/KURSOR.LST), [KURSOR.BAS](Komputer/1988/1/KURSOR.BAS), [PODZIAL.LST](Komputer/1988/1/PODZIAL.LST), [PODZIAL.BAS](Komputer/1988/1/PODZIAL.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 2

![Komputer 1988 / 2](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_02_male.jpg)

All listings on Atari disk image: [komputer_1988_2.atr](Komputer/1988/2/komputer_1988_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 23 | Mikroprogramy dla Atari XE/XL | Tadeusz Pluta, Dariusz Lelek | [KASOWNIK.LST](Komputer/1988/2/KASOWNIK.LST), [KASOWNIK.BAS](Komputer/1988/2/KASOWNIK.BAS), [KONWERT.LST](Komputer/1988/2/KONWERT.LST), [KONWERT.BAS](Komputer/1988/2/KONWERT.BAS) |
| 35 | Forum | Krzysztof Olszewski | [KOREKTOR.LST](Komputer/1988/2/KOREKTOR.LST), [KOREKTOR.BAS](Komputer/1988/2/KOREKTOR.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 3

![Komputer 1988 / 3](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_03_male.jpg)

All listings on Atari disk image: [komputer_1988_3.atr](Komputer/1988/3/komputer_1988_3.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 20 | Mikroprogramy dla Atari XE/XL | Krystian Tomczyk, Tomasz Mazur | [PIANO.LST](Komputer/1988/3/PIANO.LST), [PIANO.BAS](Komputer/1988/3/PIANO.BAS), [DIR.LST](Komputer/1988/3/DIR.LST), [DIR.BAS](Komputer/1988/3/DIR.BAS), [GR8DEMO.LST](Komputer/1988/3/GR8DEMO.LST), [GR8DEMO.BAS](Komputer/1988/3/GR8DEMO.BAS), [AUTORUN.LST](Komputer/1988/3/AUTORUN.LST), [AUTORUN.BAS](Komputer/1988/3/AUTORUN.BAS) |


[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 4

![Komputer 1988 / 4](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_04_male.jpg)

All listings on Atari disk image: [komputer_1988_4.atr](Komputer/1988/4/komputer_1988_4.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 25 | Mikroprogramy dla Atari XE/XL | Tomasz Marszałek, Mariusz Szpak | [LITERKI.LST](Komputer/1988/4/LITERKI.LST), [LITERKI.BAS](Komputer/1988/4/LITERKI.BAS), [TEKST.LST](Komputer/1988/4/TEKST.LST), [TEKST.BAS](Komputer/1988/4/TEKST.BAS), [RESET.LST](Komputer/1988/4/RESET.LST), [RESET.BAS](Komputer/1988/4/RESET.BAS) |
| 30 | Forum | Tomasz Świętosławski, Maciej Stanusch | [NAPIS.LST](Komputer/1988/4/NAPIS.LST), [NAPIS.BAS](Komputer/1988/4/NAPIS.BAS), [ZABEZP0.LST](Komputer/1988/4/ZABEZP0.LST), [ZABEZP0.BAS](Komputer/1988/4/ZABEZP0.BAS), [ZABEZP1.LST](Komputer/1988/4/ZABEZP1.LST), [ZABEZP1.BAS](Komputer/1988/4/ZABEZP1.BAS), [ZABEZP2.LST](Komputer/1988/4/ZABEZP2.LST), [ZABEZP2.BAS](Komputer/1988/4/ZABEZP2.BAS), [ZABEZP3.LST](Komputer/1988/4/ZABEZP3.LST), [ZABEZP3.BAS](Komputer/1988/4/ZABEZP3.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 5

![Komputer 1988 / 5](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_05_male.jpg)

All listings on Atari disk image: [komputer_1988_5.atr](Komputer/1988/5/komputer_1988_5.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 28 | Forum | Andrzej Lech | [RESET.LST](Komputer/1988/5/RESET.LST), [RESET.BAS](Komputer/1988/5/RESET.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 6

![Komputer 1988 / 6](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_06_male.jpg)

All listings on Atari disk image: [komputer_1988_6.atr](Komputer/1988/6/komputer_1988_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 20 | Mikroprogramy dla Atari XE/XL | Krzysztof Lewandowski, Krzysztof Możdżeń | [GENER.LST](Komputer/1988/6/GENER.LST), [GENER.BAS](Komputer/1988/6/GENER.BAS), [DEMO.LST](Komputer/1988/6/DEMO.LST), [DEMO.BAS](Komputer/1988/6/DEMO.BAS), [DZIEN.LST](Komputer/1988/6/DZIEN.LST), [DZIEN.BAS](Komputer/1988/6/DZIEN.BAS) |
| 25 | Forum | Dorota Ciesielska | [LICZBY1.LST](Komputer/1988/6/LICZBY1.LST), [LICZBY1.BAS](Komputer/1988/6/LICZBY1.BAS), [LICZBY2.LST](Komputer/1988/6/LICZBY2.LST), [LICZBY2.BAS](Komputer/1988/6/LICZBY2.BAS), [LICZBY3.LST](Komputer/1988/6/LICZBY3.LST), [LICZBY3.BAS](Komputer/1988/6/LICZBY3.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 7

![Komputer 1988 / 7](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_07_male.jpg)

All listings on Atari disk image: [komputer_1988_7.atr](Komputer/1988/7/komputer_1988_7.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 25 | Programiki dla Atari XE/XL | Mirosław Matlęga | [WIELOM.LST](Komputer/1988/7/WIELOM.LST), [WIELOM.BAS](Komputer/1988/7/WIELOM.BAS), [RAINBOW.LST](Komputer/1988/7/RAINBOW.LST), [RAINBOW.BAS](Komputer/1988/7/RAINBOW.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 9

![Komputer 1988 / 9](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_09_male.jpg)

All listings on Atari disk image: [komputer_1988_9.atr](Komputer/1988/9/komputer_1988_9.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 23 | Centronics dla Atari XE/XL | Janusz B. Wiśniewski | [CENTR1.LST](Komputer/1988/9/CENTR1.LST), [CENTR1.BAS](Komputer/1988/9/CENTR1.BAS), [CENTR2.LST](Komputer/1988/9/CENTR2.LST), [CENTR2.BAS](Komputer/1988/9/CENTR2.BAS), [CENTR3.LST](Komputer/1988/9/CENTR3.LST), [CENTR3.BAS](Komputer/1988/9/CENTR3.BAS) |
| 25 | Programiki dla Atari XE/XL | Andrzej Samojednik, Tomasz Jaworski, Piotr Bulczak | [KODY.LST](Komputer/1988/9/KODY.LST), [KODY.BAS](Komputer/1988/9/KODY.BAS), [RESET.LST](Komputer/1988/9/RESET.LST), [RESET.BAS](Komputer/1988/9/RESET.BAS), [TEXT.LST](Komputer/1988/9/TEXT.LST), [TEXT.BAS](Komputer/1988/9/TEXT.BAS), [SPEED.LST](Komputer/1988/9/SPEED.LST), [SPEED.BAS](Komputer/1988/9/SPEED.BAS) |
| 31 | Forum | Krzysztof Kobus | [DECHEX.LST](Komputer/1988/9/DECHEX.LST), [DECHEX.BAS](Komputer/1988/9/DECHEX.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 10

![Komputer 1988 / 10](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_10_male.jpg)

All listings on Atari disk image: [komputer_1988_10.atr](Komputer/1988/10/komputer_1988_10.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 16 | Mikroprogramy dla Atari XE/XL | Leszek Taratuta, Paweł Kozieł | [PROG128.LST](Komputer/1988/10/PROG128.LST), [PROG128.BAS](Komputer/1988/10/PROG128.BAS), [FALA.LST](Komputer/1988/10/FALA.LST), [FALA.BAS](Komputer/1988/10/FALA.BAS), [TECZA.LST](Komputer/1988/10/TECZA.LST), [TECZA.BAS](Komputer/1988/10/TECZA.BAS), [TEST.LST](Komputer/1988/10/TEST.LST), [TEST.BAS](Komputer/1988/10/TEST.BAS) |
| 23 | Forum | Maurycy Mikulski | [RESET.LST](Komputer/1988/10/RESET.LST), [RESET.BAS](Komputer/1988/10/RESET.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 11

![Komputer 1988 / 11](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_11_male.jpg)

All listings on Atari disk image: [komputer_1988_11.atr](Komputer/1988/11/komputer_1988_11.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 14 | Kurier | Tadeusz Olenderek | [SORT.LST](Komputer/1988/11/SORT.LST), [SORT.BAS](Komputer/1988/11/SORT.BAS) |
| 18 | Komputer dla medyka | Ryszard Tadeusiewicz, Andrzej Izworski | [SREDNIA.LST](Komputer/1988/11/SREDNIA.LST), [SREDNIA.BAS](Komputer/1988/11/SREDNIA.BAS) |
| 21 | Mikroprogramy dla Atari XL/XE | Krzysztof Rejzner, Ryszard Milewicz | [COPY484.LST](Komputer/1988/11/COPY484.LST), [COPY484.BAS](Komputer/1988/11/COPY484.BAS), [KURSOR.LST](Komputer/1988/11/KURSOR.LST), [KURSOR.BAS](Komputer/1988/11/KURSOR.BAS), [FIGURY.LST](Komputer/1988/11/FIGURY.LST), [FIGURY.BAS](Komputer/1988/11/FIGURY.BAS), [KRAJOBR.LST](Komputer/1988/11/KRAJOBR.LST), [KRAJOBR.BAS](Komputer/1988/11/KRAJOBR.BAS) |
| 26 | Forum | Paweł Smela, Maciej Stanusch | [OKNA.LST](Komputer/1988/11/OKNA.LST), [OKNA.BAS](Komputer/1988/11/OKNA.BAS), [DYSKMENU.LST](Komputer/1988/11/DYSKMENU.LST), [DYSKMENU.BAS](Komputer/1988/11/DYSKMENU.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1988 / 12

![Komputer 1988 / 12](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1988_12_male.jpg)

All listings on Atari disk image: [komputer_1988_12.atr](Komputer/1988/12/komputer_1988_12.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 20 | Komputer dla medyka | Ryszard Tadeusiewicz, Andrzej Izworski | [SYSTEM.LST](Komputer/1988/12/SYSTEM.LST), [SYSTEM.BAS](Komputer/1988/12/SYSTEM.BAS) |
| 29 | Forum | Waldemar Kudrzycki | [SILNIA.LST](Komputer/1988/12/SILNIA.LST), [SILNIA.BAS](Komputer/1988/12/SILNIA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1989 / 1

![Komputer 1989 / 1](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1989_01_male.jpg)

All listings on Atari disk image: [komputer_1989_1.atr](Komputer/1989/1/komputer_1989_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 35 | Forum | Jacek Mędrzycki | [POLLIT.PAS](Komputer/1989/1/POLLIT.PAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1989 / 2

![Komputer 1989 / 2](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1989_02_male.jpg)

All listings on Atari disk image: [komputer_1989_2.atr](Komputer/1989/2/komputer_1989_2.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 20 | Programiki dla Atari XE/XL | Jarek Parchański, Bartek Brzozowski, Przemek Widziewicz, Rysiek Rurak | [DRUKARZ.LST](Komputer/1989/2/DRUKARZ.LST), [DRUKARZ.BAS](Komputer/1989/2/DRUKARZ.BAS), [DEMO.LST](Komputer/1989/2/DEMO.LST), [DEMO.BAS](Komputer/1989/2/DEMO.BAS), [SKOCZEK.LST](Komputer/1989/2/SKOCZEK.LST), [SKOCZEK.BAS](Komputer/1989/2/SKOCZEK.BAS), [POMIAR.LST](Komputer/1989/2/POMIAR.LST), [POMIAR.BAS](Komputer/1989/2/POMIAR.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1989 / 3

![Komputer 1989 / 3](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1989_03_male.jpg)

All listings on Atari disk image: [komputer_1989_3.atr](Komputer/1989/3/komputer_1989_3.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 24 | Programiki dla Atari XE/XL | Andrzej, Marek i Wojtek, Maciek Wieczorek, Andrzej Postrzednik, Paweł Wojtysiak | [PAMIEC.LST](Komputer/1989/3/PAMIEC.LST), [PAMIEC.BAS](Komputer/1989/3/PAMIEC.BAS), [ZNAK.LST](Komputer/1989/3/ZNAK.LST), [ZNAK.BAS](Komputer/1989/3/ZNAK.BAS), [ERROR.LST](Komputer/1989/3/ERROR.LST), [ERROR.BAS](Komputer/1989/3/ERROR.BAS), [DATA.LST](Komputer/1989/3/DATA.LST), [DATA.BAS](Komputer/1989/3/DATA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Komputer 1989 / 4

![Komputer 1989 / 4](http://atarionline.pl/biblioteka/czasopisma/Komputer/Komputer_1989_04_male.jpg)

All listings on Atari disk image: [komputer_1989_4.atr](Komputer/1989/4/komputer_1989_4.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 24 | Forum | Arkadiusz Lew-Kiedrowski | [HARDCOPY.LST](Komputer/1989/4/HARDCOPY.LST), [HARDCOPY.BAS](Komputer/1989/4/HARDCOPY.BAS) |

[Go to Table of contents](#table-of-contents)

## Moje Atari

All scanned issues of **Moje Atari** magazine are available on [atarionline.pl](http://atarionline.pl/v01/index.php?subaction=showfull&id=1234027498&archive=&start_from=0&ucat=8&ct=biblioteka#zin=Moje_Atari__rok=Wszystkie)\
All listings stored on Atari disk images are available here: [moje_atari.zip](Moje_Atari/moje_atari.zip)

### Moje Atari 1990 / 1

![Moje Atari 1990 / 1](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1990_01_male.jpg)

All listings on Atari disk image: [moje_atari_1.atr](Moje_Atari/1/moje_atari_1.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 3 | Edytor Basica | | [EDYTBAS.LST](Moje_Atari/1/EDYTBAS.LST), [EDYTBAS.BAS](Moje_Atari/1/EDYTBAS.BAS) |
| 6 | Gra w linie | Andrzej Biazik | [LINIE.LST](Moje_Atari/1/LINIE.LST), [LINIE.BAS](Moje_Atari/1/LINIE.BAS) |
| 8 | Chars Designer | Andrzej Piotr Szyndrowski | [CHARSDES.LST](Moje_Atari/1/CHARSDES.LST), [CHARSDES.BAS](Moje_Atari/1/CHARSDES.BAS), [CHDLOAD.LST](Moje_Atari/1/CHDLOAD.LST), [CHDLOAD.BAS](Moje_Atari/1/CHDLOAD.BAS) |
| 12 | Okna raz jeszcze | L. Pasternak, Janusz Pelc | [OKNA.LST](Moje_Atari/1/OKNA.LST), [OKNA.BAS](Moje_Atari/1/OKNA.BAS) |
| 14 | Pionowe wahanie obrazu | Adam Segert | [WAHANIE.LST](Moje_Atari/1/WAHANIE.LST), [WAHANIE.BAS](Moje_Atari/1/WAHANIE.BAS) |
| 19 | Ocena - Alfabet Morse'a | Tomek Czyżew, Wojciech Zientara | [MORSE1.LST](Moje_Atari/1/MORSE1.LST), [MORSE1.BAS](Moje_Atari/1/MORSE1.BAS), [MORSE2.LST](Moje_Atari/1/MORSE2.LST), [MORSE2.BAS](Moje_Atari/1/MORSE2.BAS), [MORSE3.LST](Moje_Atari/1/MORSE3.LST), [MORSE3.BAS](Moje_Atari/1/MORSE3.BAS) |
| 27 | PMG Generator | Jakub Cebula | [PMGGEN.LST](Moje_Atari/1/PMGGEN.LST), [PMGGEN.BAS](Moje_Atari/1/PMGGEN.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1990 / 2

![Moje Atari 1990 / 2](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1990_02_male.jpg)

All listings on Atari disk images: [moje_atari_2a.atr](Moje_Atari/2/moje_atari_2a.atr), [moje_atari_2b.atr](Moje_Atari/2/moje_atari_2b.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 4 | Edytor Basica | | [EDYTBAS.LST](Moje_Atari/2/EDYTBAS.LST), [EDYTBAS.BAS](Moje_Atari/2/EDYTBAS.BAS) |
| 6 | Gra w kości | Leszek Stróżowski | [KOSCI.LST](Moje_Atari/2/KOSCI.LST), [KOSCI.BAS](Moje_Atari/2/KOSCI.BAS) |
| 10 | Jeszcze raz o duszkach | Andrzej Biazik | [PMG1.LST](Moje_Atari/2/PMG1.LST), [PMG1.BAS](Moje_Atari/2/PMG1.BAS), [PMG1I2.LST](Moje_Atari/2/PMG1I2.LST), [PMG1I2.BAS](Moje_Atari/2/PMG1I2.BAS) |
| 12 | Multi DOS | Krzysztof Klimczak | [MULTDOS.LST](Moje_Atari/2/MULTDOS.LST), [MULTDOS.BAS](Moje_Atari/2/MULTDOS.BAS) |
| 19 | Ocena - Wyścig kotów | Andrzej Reczuch, Wojciech Zientara | [WYSCIG1.LST](Moje_Atari/2/WYSCIG1.LST), [WYSCIG1.BAS](Moje_Atari/2/WYSCIG1.BAS), [WYSCIG2.LST](Moje_Atari/2/WYSCIG2.LST), [WYSCIG2.BAS](Moje_Atari/2/WYSCIG2.BAS) |
| 20 | Pchełka | | [PCHELKA.LST](Moje_Atari/2/PCHELKA.LST), [PCHELKA.BAS](Moje_Atari/2/PCHELKA.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 3

![Moje Atari 1991 / 3](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_03_male.jpg)

All listings on Atari disk images: [moje_atari_3a.atr](Moje_Atari/3/moje_atari_3a.atr), [moje_atari_3b.atr](Moje_Atari/3/moje_atari_3b.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje_Atari/3/EDYTBAS.LST), [EDYTBAS.BAS](Moje_Atari/3/EDYTBAS.BAS) |
| 7 | Business Man | Adam Wasylewski | [BUSINES1.LST](Moje_Atari/3/BUSINES1.LST), [BUSINESS.GRA](Moje_Atari/3/BUSINESS.GRA), [BUSINES2.LST](Moje_Atari/3/BUSINES2.LST), [BUSINESS.POM](Moje_Atari/3/BUSINESS.POM), [BUSINES3.LST](Moje_Atari/3/BUSINES3.LST), [BUSINES4.LST](Moje_Atari/3/BUSINES4.LST) |
| 9 | Long File Copy | Tomasz Bielak | [LFC.LST](Moje_Atari/3/LFC.LST), [LFC.BAS](Moje_Atari/3/LFC.BAS) |
| 12 | Kwasy | Krzysztof Przybylak | [KWASY.LST](Moje_Atari/3/KWASY.LST), [KWASY.BAS](Moje_Atari/3/KWASY.BAS) |
| 14 | Mini GEM | Radosław Korga | [GEM1.LST](Moje_Atari/3/GEM1.LST), [GEM1.BAS](Moje_Atari/3/GEM1.BAS), [GEM1I2.LST](Moje_Atari/3/GEM1I2.LST), [GEM1I2.BAS](Moje_Atari/3/GEM1I2.BAS), [GEM1I3.LST](Moje_Atari/3/GEM1I3.LST), [GEM1I3.BAS](Moje_Atari/3/GEM1I3.BAS) |
| 23 | Ocena - Wykresy | Rafał Golba, Wojciech Zientara | [WYKRESY1.LST](Moje_Atari/3/WYKRESY1.LST), [WYKRESY1.BAS](Moje_Atari/3/WYKRESY1.BAS), [WYKRESY2.LST](Moje_Atari/3/WYKRESY2.LST), [WYKRESY2.BAS](Moje_Atari/3/WYKRESY2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 4

![Moje Atari 1991 / 4](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_04_male.jpg)

All listings on Atari disk images: [moje_atari_4a.atr](Moje_Atari/4/moje_atari_4a.atr), [moje_atari_4b.atr](Moje_Atari/4/moje_atari_4b.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje_Atari/4/EDYTBAS.LST), [EDYTBAS.BAS](Moje_Atari/4/EDYTBAS.BAS) |
| 6 | Trening na klawiaturze | Konrad Makarewicz | [TRENING.LST](Moje_Atari/4/TRENING.LST), [TRENING.BAS](Moje_Atari/4/TRENING.BAS) |
| 8 | Test RAM | Marek Zachar | [TESTRAM.LST](Moje_Atari/4/TESTRAM.LST), [TESTRAM.BAS](Moje_Atari/4/TESTRAM.BAS) |
| 11 | Tylko dla zaawansowanych | Andrzej Zalewski | [ZAAWANS1.LST](Moje_Atari/4/ZAAWANS1.LST), [ZAAWANS1.BAS](Moje_Atari/4/ZAAWANS1.BAS), [ZAAWANS3.LST](Moje_Atari/4/ZAAWANS3.LST), [ZAAWANS3.BAS](Moje_Atari/4/ZAAWANS3.BAS), [ZAAWANS5.LST](Moje_Atari/4/ZAAWANS5.LST), [ZAAWANS5.BAS](Moje_Atari/4/ZAAWANS5.BAS) |
| 14 | Suplement do Chars Designer | Marek Kulczycki, Andrzej Piotr Szyndrowski | [CHDLOAD2.LST](Moje_Atari/4/CHDLOAD2.LST), [CHDLOAD2.BAS](Moje_Atari/4/CHDLOAD2.BAS), [CHARSDE2.LST](Moje_Atari/4/CHARSDE2.LST), [CHARSDE2.BAS](Moje_Atari/4/CHARSDE2.BAS) |
| 14 | Hard Copy | Arkadiusz Lew-Kiedrowski | [HARDCOPY.LST](Moje_Atari/4/HARDCOPY.LST), [HARDCOPY.BAS](Moje_Atari/4/HARDCOPY.BAS) |
| 19 | Ocena - Matematyka | Paweł Tutka, Wojciech Zientara | [MATEMAT1.LST](Moje_Atari/4/MATEMAT1.LST), [MATEMAT1.BAS](Moje_Atari/4/MATEMAT1.BAS), [MATEMAT2.LST](Moje_Atari/4/MATEMAT2.LST), [MATEMAT2.BAS](Moje_Atari/4/MATEMAT2.BAS), [MATEMAT3.LST](Moje_Atari/4/MATEMAT3.LST), [MATEMAT3.BAS](Moje_Atari/4/MATEMAT3.BAS) |
| 24 | Wykresy funkcji | Jacek Tarasiuk | [WYKRESY.LST](Moje_Atari/4/WYKRESY.LST), [WYKRESY.BAS](Moje_Atari/4/WYKRESY.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 5

![Moje Atari 1991 / 5](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_05_male.jpg)

All listings on Atari disk image: [moje_atari_5.atr](Moje_Atari/5/moje_atari_5.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje_Atari/5/EDYTBAS.LST), [EDYTBAS.BAS](Moje_Atari/5/EDYTBAS.BAS) |
| 6 | Robot R-29 | Tomasz Rogacewicz | [ROBOT.LST](Moje_Atari/5/ROBOT.LST), [ROBOT.BAS](Moje_Atari/5/ROBOT.BAS) |
| 8 | Analiza stałoprądowa | Jacek Tarasiuk | [ANALIZA.LST](Moje_Atari/5/ANALIZA.LST), [ANALIZA.BAS](Moje_Atari/5/ANALIZA.BAS) |
| 12 | Projektowanie programu ANTICA | Jacek Tarasiuk | [ANTIC.LST](Moje_Atari/5/ANTIC.LST), [ANTIC.BAS](Moje_Atari/5/ANTIC.BAS) |
| 13 | Tylko dla zaawansowanych | Andrzej Zalewski | [ZAAWANS1.LST](Moje_Atari/5/ZAAWANS1.LST), [ZAAWANS1.BAS](Moje_Atari/5/ZAAWANS1.BAS), [ZAAWANS2.LST](Moje_Atari/5/ZAAWANS2.LST), [ZAAWANS2.BAS](Moje_Atari/5/ZAAWANS2.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 6

![Moje Atari 1991 / 6](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_06_male.jpg)

All listings on Atari disk image: [moje_atari_6.atr](Moje_Atari/6/moje_atari_6.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje_Atari/6/EDYTBAS.LST), [EDYTBAS.BAS](Moje_Atari/6/EDYTBAS.BAS) |
| 7 | Wojny rdzeniowe | Wojciech Zientara | [WOJNY.LST](Moje_Atari/6/WOJNY.LST), [WOJNY.BAS](Moje_Atari/6/WOJNY.BAS), [SKOCZEK.CW](Moje_Atari/6/SKOCZEK.CW), [BLIZNIAK.CW](Moje_Atari/6/BLIZNIAK.CW), [KARZEL.CW](Moje_Atari/6/KARZEL.CW), [ZMIENIA.CW](Moje_Atari/6/ZMIENIA.CW), [KOPIER.CW](Moje_Atari/6/KOPIER.CW), [ROBAK.CW](Moje_Atari/6/ROBAK.CW), [SKOCZZBR.CW](Moje_Atari/6/SKOCZZBR.CW), [BLIZNZBR.CW](Moje_Atari/6/BLIZNZBR.CW) |
| 9 | Katalog dyskietki | Jarosław Kuliński | [KATALOG.LST](Moje_Atari/6/KATALOG.LST), [KATALOG.BAS](Moje_Atari/6/KATALOG.BAS) |
| 11 | Krótkie, krótsze, najkrótsze | Wojciech Zientara | [RZYMSKIE.LST](Moje_Atari/6/RZYMSKIE.LST), [RZYMSKIE.BAS](Moje_Atari/6/RZYMSKIE.BAS), [KONTROLA.LST](Moje_Atari/6/KONTROLA.LST), [KONTROLA.BAS](Moje_Atari/6/KONTROLA.BAS), [RESET.LST](Moje_Atari/6/RESET.LST), [RESET.BAS](Moje_Atari/6/RESET.BAS), [GETKEY.LST](Moje_Atari/6/GETKEY.LST), [GETKEY.BAS](Moje_Atari/6/GETKEY.BAS), [LINIJKA.LST](Moje_Atari/6/LINIJKA.LST), [LINIJKA.BAS](Moje_Atari/6/LINIJKA.BAS) |
| 12 | Duszki w natarciu | Jakub Cebula | [DUSZKI1.LST](Moje_Atari/6/DUSZKI1.LST), [DUSZKI1.BAS](Moje_Atari/6/DUSZKI1.BAS), [DUSZKI2.LST](Moje_Atari/6/DUSZKI2.LST), [DUSZKI2.BAS](Moje_Atari/6/DUSZKI2.BAS), [DUSZKI3.LST](Moje_Atari/6/DUSZKI3.LST), [DUSZKI3.BAS](Moje_Atari/6/DUSZKI3.BAS) |
| 13 | Tylko dla zaawansowanych | Andrzej Zalewski | [ZAAWANS.LST](Moje_Atari/6/ZAAWANS.LST), [ZAAWANS.BAS](Moje_Atari/6/ZAAWANS.BAS) |
| 17 | Atari jako sterownik | Marek Ruta | [STEROWN.LST](Moje_Atari/6/STEROWN.LST), [STEROWN.BAS](Moje_Atari/6/STEROWN.BAS) |
| 21 | Zapis i odczyt rysunków Koali | Krzysztof Klimczak | [KOALA1.LST](Moje_Atari/6/KOALA1.LST), [KOALA1.BAS](Moje_Atari/6/KOALA1.BAS), [KOALA1I2.LST](Moje_Atari/6/KOALA1I2.LST), [KOALA1I2.BAS](Moje_Atari/6/KOALA1I2.BAS), [KOALA3.LST](Moje_Atari/6/KOALA3.LST), [KOALA3.BAS](Moje_Atari/6/KOALA3.BAS), [KOALA3I4.LST](Moje_Atari/6/KOALA3I4.LST), [KOALA3I4.BAS](Moje_Atari/6/KOALA3I4.BAS) |

[Go to Table of contents](#table-of-contents)

---
### Moje Atari 1991 / 7

![Moje Atari 1991 / 7](http://atarionline.pl/biblioteka/czasopisma/Moje_Atari/Moje_Atari_1991_07_male.jpg)

All listings on Atari disk image: [moje_atari_7.atr](Moje_Atari/7/moje_atari_7.atr)

| Page | Title | Author | Files |
| ------ | ------ | ------ | ------ | 
| 5 | Edytor Basica | | [EDYTBAS.LST](Moje_Atari/7/EDYTBAS.LST), [EDYTBAS.BAS](Moje_Atari/7/EDYTBAS.BAS) |
| 6 | Starcie | Jarosław Jacek | [STARCIE.LST](Moje_Atari/7/STARCIE.LST), [STARCIE.BAS](Moje_Atari/7/STARCIE.BAS) |
| 8 | Biblioteka rozdań brydżowych | Piotr Kastarynda | [BRYDZ.LST](Moje_Atari/7/BRYDZ.LST), [BRYDZ.BAS](Moje_Atari/7/BRYDZ.BAS) |
| 9 | Kasetowa baza danych | Jakub Cebula | [BASBAZ.LST](Moje_Atari/7/BASBAZ.LST), [BASBAZ.BAS](Moje_Atari/7/BASBAZ.BAS), [BASBAZ2.LST](Moje_Atari/7/BASBAZ2.LST), [BASBAZ2.BAS](Moje_Atari/7/BASBAZ2.BAS) |
| 12 | Tylko dla zaawansowanych | Andrzej Zalewski | [ZAAWANS1.LST](Moje_Atari/7/ZAAWANS1.LST), [ZAAWANS1.BAS](Moje_Atari/7/ZAAWANS1.BAS), [ZAAWANS2.LST](Moje_Atari/7/ZAAWANS2.LST), [ZAAWANS2.BAS](Moje_Atari/7/ZAAWANS2.BAS), [ZAAWANS3.LST](Moje_Atari/7/ZAAWANS3.LST), [ZAAWANS3.BAS](Moje_Atari/7/ZAAWANS3.BAS) |
| 15 | Ramdisk 130XE | Marek Omirski | [RAMDYSK.LST](Moje_Atari/7/RAMDYSK.LST), [RAMDYSK.BAS](Moje_Atari/7/RAMDYSK.BAS) |

[Go to Table of contents](#table-of-contents)
